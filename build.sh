#!/bin/sh

if [ -z $LAR_SIMLATOR_MAIN_DIR ] ; then
    echo '$LAR_SIMLATOR_MAIN_DIR has not been defined! Execute source setup.sh in the main directory.'
    return 2>&- || exit
fi

myhost=`hostname`
if [[ $myhost == login*.icepp.jp || $myhost == lxplus*.cern.ch || $myhost == pcatutt28 ]] ; then
    lsetup "views LCG_98python3 x86_64-centos7-gcc9-opt"
fi

git submodule update -i

PLISM_BUILD_DIR=$LAR_SIMLATOR_MAIN_DIR/build
if [ ! -e $PLISM_BUILD_DIR/Makefile ] ; then
    cmake -S $LAR_SIMLATOR_MAIN_DIR/source/ -B $PLISM_BUILD_DIR
fi

cmake --build $PLISM_BUILD_DIR -j \
    && mv -f $PLISM_BUILD_DIR/CellPropertyGenerator/src/*Module* $LAR_SIMLATOR_MAIN_DIR/run/tool \
    && mv -f $PLISM_BUILD_DIR/HitSummarizer/src/*Module* $LAR_SIMLATOR_MAIN_DIR/run/tool \
    && mv -f $PLISM_BUILD_DIR/SequenceGenerator/src/*Module* $LAR_SIMLATOR_MAIN_DIR/run/tool \
    && mv -f $PLISM_BUILD_DIR/Reconstructor/src/*Module* $LAR_SIMLATOR_MAIN_DIR/run/tool \
    && mv -f $PLISM_BUILD_DIR/SCconditionSimulator/src/*Module* $LAR_SIMLATOR_MAIN_DIR/run/tool
    #&& mv -f $PLISM_BUILD_DIR/Analysis/Plotter/src/*Module* $LAR_SIMLATOR_MAIN_DIR/run/tool


cd $LAR_SIMLATOR_MAIN_DIR
