#include <TCanvas.h>
#include <TLine.h>

#include <iomanip>

#include "../AtlasStyle/inc/AtlasStyle.hpp"
#include "../DataBase/inc/DataBase.hpp"
#include "../Generator/inc/Generator.hpp"
#include "../Plotter/inc/Plotter.hpp"
#include "BaselineCorrectedDataBase/inc/BaselineCorrectedDataBase.hpp"
#include "Function.hpp"
int main(int argc, char const* argv[])
{
    PLISMAnalysis::BaselineCorrectedDataBase d{argv[1]};

    std::string title{
        "Baseline shift (#mu = " + std::to_string(static_cast<int>(d.Mus[0])) +
        ", Train = " + PLISMAnalysis::toStringTrainPattern(d.TrainPatterns[0]) +
        "); [th BC]; E_{T} [ADC]"};
    std::string of{"Baseline.pdf"};

    // get Data
    auto&& DigitBaseline = d.getDigitBaseline()[0];
    auto&& CorrectedRecEt = d.getBaselineCorrectedRecEts()[0];
    auto&& CorrectedRecTau = d.getBaselineCorrectedRecTaus()[0];
    auto&& CorrectedRecEtTau = d.getBaselineCorrectedRecEtTaus()[0];
    auto&& Period_BC = d.Periods_BC[0];

    double yup{__DBL_MIN__};
    double ylow{__DBL_MAX__};
    const double xlow{0.};
    const double xup{static_cast<double>(d.Periods_BC[0]) - 1};

    TCanvas c{"c", ""};
    c.SetLeftMargin(0.2);
    TGraph g{title.c_str()};
    for (unsigned int BC = 0; BC < DigitBaseline.size(); BC++) {
        g.SetPoint(g.GetN(), BC, DigitBaseline[BC]);
        yup = std::max(DigitBaseline[BC], yup);
        ylow = std::min(DigitBaseline[BC], ylow);
    }
    yup += 0.1 * std::abs(yup);
    ylow -= 0.1 * std::abs(ylow);

    g.SetMaximum(yup);
    g.SetMinimum(ylow);
    g.GetXaxis()->SetLimits(xlow, xup);
    g.SetLineColor(kBlue);
    g.Draw("al");

    // Tline
    int t{0};
    std::vector<TLine> vtl;
    for (int i = 0; i < d.TrainPatterns[0].size(); i++) {
        t += d.TrainPatterns[0][i];
        vtl.emplace_back(t - 1, ylow, t - 1, yup);
    }
    for (auto& tl : vtl) {
        tl.SetLineStyle(3);
        tl.Draw();
    }

    c.Print(of.c_str());
    c.Close();
}