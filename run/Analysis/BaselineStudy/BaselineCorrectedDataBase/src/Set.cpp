#include "../inc/BaselineCorrectedDataBase.hpp"

namespace PLISMAnalysis
{
    const int NumBitsForCorrection{10};
    const int NumBCsForCorrection{1 << NumBitsForCorrection};
    void BaselineCorrectedDataBase::SetDigits()
    {
        for (unsigned int label = 0; label < InputFiles.size(); label++) {
            auto&& fin = InputFiles[label];
            TTreeReader reader{InputFileTypes[label].c_str(), fin};
            TTreeReaderValue<std::deque<int>> Digit{reader, "Digit"};
            reader.Next();

            long long _BC{NumBCsForCorrection * Periods_BC[label]};
            for (long long BC = 0; BC < _BC; BC++) { (*Digit).pop_front(); }
            Digits.push_back(*Digit);
        }
    }

    void BaselineCorrectedDataBase::SetDigitBaselines()
    {
        for (unsigned int label = 0; label < InputFiles.size(); label++) {
            auto&& fin = InputFiles[label];
            TTreeReader reader{InputFileTypes[label].c_str(), fin};
            TTreeReaderValue<std::deque<int>> Digit{reader, "Digit"};
            reader.Next();

            std::vector<double> DigitBaseline(Periods_BC[label]);
            for (long long BC = 0; BC < Periods_BC[label]; BC++) {
                long long sum{0};
                for (auto _BC = BC;
                     _BC < NumBCsForCorrection * Periods_BC[label];
                     _BC += Periods_BC[label]) {
                    sum += (*Digit)[_BC];
                }

                if (sum >= 0) {
                    DigitBaseline[BC] =
                        (sum >> NumBitsForCorrection) +
                        ((sum >> (NumBitsForCorrection - 1)) & 1) * 0.5 +
                        ((sum >> (NumBitsForCorrection - 2)) & 1) * 0.25 +
                        ((sum >> (NumBitsForCorrection - 3)) & 1) * 0.125;
                } else {
                    sum = ~sum + 1;  // sum = - sum
                    DigitBaseline[BC] =
                        -((sum >> NumBitsForCorrection) +
                          ((sum >> (NumBitsForCorrection - 1)) & 1) * 0.5 +
                          ((sum >> (NumBitsForCorrection - 2)) & 1) * 0.25 +
                          ((sum >> (NumBitsForCorrection - 3)) & 1) * 0.125);
                }

                /*
               DigitBaseline[BC] = static_cast<double>(sum) /
                                   static_cast<double>(NumBCsForCorrection);
                                   */
            }
            DigitBaselines.push_back(DigitBaseline);
        }
    }

    void BaselineCorrectedDataBase::SetBaselineCorrectedDigits()
    {
        for (unsigned int label = 0; label < InputFiles.size(); label++) {
            std::vector<double> BaselineCorrectedDigit{};
            for (unsigned long long BC = 0; BC < Digits[label].size(); BC++) {
                BaselineCorrectedDigit.push_back(
                    static_cast<double>(Digits[label][BC]) -
                    DigitBaselines[label][BC % Periods_BC[label]]);
            }
            BaselineCorrectedDigits.push_back(BaselineCorrectedDigit);
        }
    }

    void BaselineCorrectedDataBase::SetBaselineCorrectedRecs()
    {
        for (unsigned int label = 0; label < InputFiles.size(); label++) {
            std::vector<double> BaselineCorrectedRecEt{};
            std::vector<double> BaselineCorrectedRecTau{};
            std::vector<double> BaselineCorrectedRecEtTau{};

            for (unsigned long long BC = 0;
                 BC + NSample < BaselineCorrectedDigits[label].size(); BC++) {
                double Et{0.0}, EtTau{0.0};
                for (unsigned long long samp = 0; samp < NSample; samp++) {
                    Et +=
                        ofc.a[samp] * BaselineCorrectedDigits[label][BC + samp];
                    EtTau +=
                        ofc.b[samp] * BaselineCorrectedDigits[label][BC + samp];
                }
                BaselineCorrectedRecEt.push_back(Et);
                BaselineCorrectedRecEtTau.push_back(EtTau);
                BaselineCorrectedRecTau.push_back(EtTau / Et);
            }
            BaselineCorrectedRecEts.push_back(BaselineCorrectedRecEt);
            BaselineCorrectedRecEtTaus.push_back(BaselineCorrectedRecEtTau);
            BaselineCorrectedRecTaus.push_back(BaselineCorrectedRecTau);
        }
    }
}  // namespace PLISMAnalysis