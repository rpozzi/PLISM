#include <TCanvas.h>
#include <TH1D.h>
#include <TLegend.h>

#include "../AtlasStyle/inc/AtlasStyle.hpp"
#include "../DataBase/inc/DataBase.hpp"
#include "../Generator/inc/Generator.hpp"
#include "../Plotter/inc/Plotter.hpp"
#include "BaselineCorrectedDataBase/inc/BaselineCorrectedDataBase.hpp"
#include "Function.hpp"

int main(int argc, char const* argv[])
{
    PLISMAnalysis::BaselineCorrectedDataBase d{argv[1]};

    for (int InputFileLabel = 0; InputFileLabel < d.getNumInputFiles();
         InputFileLabel++) {
        std::string tp{""};
        const auto& TrainPattern = d.TrainPatterns[InputFileLabel];
        int ctr{0};
        for (auto it = TrainPattern.begin(); it != TrainPattern.end();
             it++, ctr++) {
            tp += std::to_string(*it);
            if (ctr % 2 == 0) {
                tp += "b";
            } else {
                tp += "e";
            }
        }
        std::string of{"EtDistributions.pdf"};

        const auto Period_BC{d.Periods_BC[InputFileLabel]};
        const auto TotalBC{d.TotalBCs[InputFileLabel]};

        TCanvas c{"c", ""};
        c.SetLeftMargin(0.2);
        c.SetLogy();

        // for read file
        auto&& fin = d.InputFiles[InputFileLabel];
        TTreeReader reader{d.InputFileTypes[InputFileLabel].c_str(), fin};
        TTreeReaderValue<std::deque<double>> AnalogEt{reader, "AnalogEt"};
        reader.Next();

        auto&& CorrectedRecEt = d.getBaselineCorrectedRecEts()[InputFileLabel];
        auto&& CorrectedRecEtTau =
            d.getBaselineCorrectedRecEtTaus()[InputFileLabel];
        auto&& CorrectedRecTau =
            d.getBaselineCorrectedRecTaus()[InputFileLabel];

        c.Print((of + "[").c_str());
        for (unsigned long long BC = 0; BC < Period_BC; BC++) {
            std::string title{
                "E_{T} distribution at " + std::to_string(BC) + "BC (#mu = " +
                std::to_string(static_cast<int>(d.Mus[InputFileLabel])) +
                ", Train =" + tp + "); E_{T} [MeV];Entries"};
            TH1D h1{"h", title.c_str(), 100, -1000, 3000};
            TH1D h2{"h", title.c_str(), 100, -1000, 3000};
            TH1D h3{"h", title.c_str(), 100, -1000, 3000};

            for (unsigned long long _BC = BC; _BC < CorrectedRecEt.size();
                 _BC += Period_BC) {
                h1.Fill(CorrectedRecEt[_BC]);
                if (PLISMAnalysis::TauCriteria(CorrectedRecTau[_BC],
                                               CorrectedRecEt[_BC])) {
                    h2.Fill(CorrectedRecEt[_BC]);
                }
                h3.Fill((*AnalogEt)[_BC]);
            }
            h1.SetStats(0);
            h2.SetStats(0);
            h3.SetStats(0);
            h1.SetFillColor(kBlue);
            h2.SetFillColor(kYellow);
            h3.SetFillColor(kGreen);
            h1.Draw("");
            h2.Draw("same");
            // Legend
            TLegend leg{0.50, 0.85, 0.65, 0.89};
            leg.SetLineColor(0);
            leg.SetFillColor(0);
            leg.SetTextSize(0.025);
            leg.AddEntry(&h1, "Reconstructed E_{T} (Baseline is corrected)",
                         "f");
            leg.AddEntry(&h2, "Selected E_{T} (Baseline is corrected)", "f");
            // leg.AddEntry(&h3, "Injected E_{T} (Baseline is uncorrected)",
            // "f");
            leg.Draw();
            c.RedrawAxis();
            c.Print(of.c_str());
        }
        c.Print((of + "]").c_str());
        c.Close();
    }
}
