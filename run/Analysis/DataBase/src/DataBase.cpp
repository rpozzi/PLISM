#include "../inc/DataBase.hpp"

namespace PLISMAnalysis
{
    DataBase::DataBase(const std::vector<std::string>& InputFilenames_)
        : BCInterval_ns(25)
    {
        auto TrainPattern =
            [&](const std::string& InputFilename) -> std::vector<int> {
            auto&& InputFilelabel = InputFilenameToLabel[InputFilename];
            auto&& fin = InputFiles[InputFilelabel];
            TTreeReader reader{InputFileTypes[InputFilelabel].c_str(), fin};
            TTreeReaderValue<std::vector<int>> tp{reader, "TrainPattern"};
            reader.Next();
            return *tp;
        };
        auto SignalPattern =
            [&](const std::string& InputFilename) -> std::vector<int> {
            auto&& InputFilelabel = InputFilenameToLabel[InputFilename];
            auto&& fin = InputFiles[InputFilelabel];
            TTreeReader reader{InputFileTypes[InputFilelabel].c_str(), fin};
            TTreeReaderValue<std::vector<int>> sp{reader, "SignalPattern"};
            reader.Next();
            return *sp;
        };
        auto TotalBC = [&](const std::string& InputFilename) -> long long {
            auto&& InputFilelabel = InputFilenameToLabel[InputFilename];
            auto&& fin = InputFiles[InputFilelabel];
            TTreeReader reader{InputFileTypes[InputFilelabel].c_str(), fin};
            TTreeReaderValue<std::vector<double>> e{reader, "SignalTrueEt"};
            reader.Next();
            return static_cast<long long>((*e).size());
        };
        auto DataType = [](const std::string& InputFilename) -> std::string {
            std::smatch m;
            if (std::regex_search(InputFilename, m,
                                  std::regex{"reconstructed"})) {
                return m.str();
            } else if (std::regex_search(InputFilename, m,
                                         std::regex{"digitSequence"})) {
                return m.str();
            } else {
                std::cerr << WarningMessage
                          << "::DataBase: "
                             "Unenable to recognize datatype !!!\n";
                return "reconstructed";
            }
        };
        auto MinMaxEt_MeV =
            [&](const std::string& InputFilename) -> std::pair<double, double> {
            auto&& InputFilelabel = InputFilenameToLabel[InputFilename];
            auto&& fin = InputFiles[InputFilelabel];
            TTreeReader reader{InputFileTypes[InputFilelabel].c_str(), fin};
            TTreeReaderValue<std::vector<double>> e{reader, "SignalTrueEt"};
            double EtMax{-__DBL_MAX__}, EtMin{__DBL_MAX__};
            while (reader.Next()) {
                for (long long BC = 0; BC < TotalBCs[InputFilelabel];) {
                    bool isEvent = true;
                    for (const auto& v : SignalPatterns[InputFilelabel]) {
                        if (isEvent) {
                            for (long long BC_ = 0;
                                 BC_ < v && BC < TotalBCs[InputFilelabel];
                                 BC++, BC_++) {
                                // std::cout << BC << std::endl;
                                EtMax = std::max(EtMax, (*e)[BC]);
                                EtMin = std::min(EtMin, (*e)[BC]);
                            }
                        } else {
                            BC += v;
                        }
                        isEvent = !isEvent;
                    }
                }
            }
            if (EtMax == -__DBL_MAX__) EtMax = 0.;
            if (EtMin == __DBL_MAX__) EtMin = 0.;
            return std::make_pair(EtMin, EtMax);
        };
        auto MinMaxTau_ns =
            [&](const std::string& InputFilename) -> std::pair<double, double> {
            auto&& InputFilelabel = InputFilenameToLabel[InputFilename];
            auto&& fin = InputFiles[InputFilelabel];
            TTreeReader reader{InputFileTypes[InputFilelabel].c_str(), fin};
            TTreeReaderValue<std::vector<double>> t{reader, "SignalTrueTau"};
            double TauMax{-__DBL_MAX__}, TauMin{__DBL_MAX__};
            while (reader.Next()) {
                for (long long BC = 0; BC < TotalBCs[InputFilelabel];) {
                    bool isEvent = true;
                    for (const auto& v : SignalPatterns[InputFilelabel]) {
                        if (isEvent) {
                            for (long long BC_ = 0;
                                 BC_ < v && BC < TotalBCs[InputFilelabel];
                                 BC++, BC_++) {
                                TauMax = std::max(TauMax, (*t)[BC]);
                                TauMin = std::min(TauMin, (*t)[BC]);
                            }
                        } else {
                            BC += v;
                        }
                        isEvent = !isEvent;
                    }
                }
            }
            if (TauMax == -__DBL_MAX__) TauMax = 0.;
            if (TauMin == __DBL_MAX__) TauMin = 0.;
            return std::make_pair(TauMin, TauMax);
        };
        auto Mu = [&](const std::string& InputFilename) -> double {
            auto&& InputFilelabel = InputFilenameToLabel[InputFilename];
            auto&& fin = InputFiles[InputFilelabel];
            TTreeReader reader{InputFileTypes[InputFilelabel].c_str(), fin};
            TTreeReaderValue<double> mu{reader, "mu"};
            reader.Next();
            return *mu;
        };

        NumInputFiles = 0;
        for (const std::string& f : InputFilenames_) {
            auto InputFile = new TFile{f.c_str(), "read"};
            if (!InputFile->IsOpen()) {
                std::cerr << WarningMessage << "::DataBase: "
                          << "Unenable to Open inputfile :" << f << "!!!\n";
                return;
            }
            InputFilenameToLabel[f] = NumInputFiles++;
            InputFilenames.push_back(f);
            InputFiles.push_back(InputFile);
            InputFileTypes.push_back(DataType(f));
            auto&& tp = TrainPattern(f);
            Periods_BC.push_back(std::accumulate(tp.begin(), tp.end(), 0));
            TrainPatterns.push_back(tp);
            SignalPatterns.push_back(SignalPattern(f));
            TotalBCs.push_back(TotalBC(f));
            MinMaxEts_MeV.push_back(MinMaxEt_MeV(f));
            MinMaxTaus_ns.push_back(MinMaxTau_ns(f));
            Mus.push_back(Mu(f));
        }
        PrintLabels();
    }
}  // namespace PLISMAnalysis