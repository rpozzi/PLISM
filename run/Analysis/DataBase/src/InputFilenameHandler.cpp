#include "../inc/DataBase.hpp"

namespace PLISMAnalysis
{
    void DataBase::InputFilenameHandler(const std::string& InputFilename)
    {
        try {
            if (InputFilenameToLabel.find(InputFilename) ==
                InputFilenameToLabel.end()) {
                throw InputFilename;
            }
        } catch (const std::string& fn) {
            std::cerr << ErrorMessage << "::InputFilenameHandler: "
                      << "cannot find " << fn << "\n";
            exit(EXIT_FAILURE);
        }
    }
}  // namespace PLISMAnalysis