#include "../inc/Generator.hpp"

namespace PLISMAnalysis
{
    std::string Generator::DefaultAxisLabel(const std::string& DataName,
                                            bool MeVtoGeV)
    {
        if (DataName == std::string{"SignalTrueEt"}) {
            if (MeVtoGeV) {
                return std::string{"True E_{T} [GeV]"};
            } else {
                return std::string{"True E_{T} [MeV]"};
            }
        } else if (DataName == std::string{"SignalTrueTau"}) {
            return std::string{"True #tau [ns]"};
        } else if (DataName == std::string{"SignalTrueEtTau"}) {
            if (MeVtoGeV) {
                return std::string{"TrueE_{T}#tau [GeV ns]"};
            } else {
                return std::string{"TrueE_{T}#tau [MeV ns]"};
            }
        } else if (DataName == std::string{"Digit"}) {
            return std::string{"Digit [ADC]"};
        } else if (DataName == std::string{"RecEt"}) {
            if (MeVtoGeV) {
                return std::string{"Reconstructed E_{T} [GeV]"};
            } else {
                return std::string{"Reconstructed E_{T} [MeV]"};
            }
        } else if (DataName == std::string{"RecTau"}) {
            return std::string{"Reconstructed #tau [ns]"};
        } else if (DataName == std::string{"RecEtTau"}) {
            if (MeVtoGeV) {
                return std::string{"Reconstructed E_{T}#tau [GeV ns]"};
            } else {
                return std::string{"Reconstructed E_{T}#tau [MeV ns]"};
            }
        } else if (DataName == std::string{"EtResolution"}) {
            return std::string{"E_{T} Resolution"};
        } else if (DataName == std::string{"TauResolution"}) {
            return std::string{"#tau Resolution"};
        } else if (DataName == std::string{"Digit"}) {
            return std::string{"Digit [ADC]"};
        } else if (DataName == std::string("DigitLSB")) {
            if (MeVtoGeV) {
                return std::string{"Digit #times LSB [GeV]"};
            } else {
                return std::string{"Digit #times LSB [MeV]"};
            }
        } else if (DataName == std::string{"AnalogEt"}) {
            if (MeVtoGeV) {
                return std::string{"AnalogE_{T} [GeV]"};
            } else {
                return std::string{"AnalogE_{T} [MeV]"};
            }
        } else if (DataName == std::string{"BgAnalogEt"}) {
            if (MeVtoGeV) {
                return std::string{"BgAnalogE_{T} [GeV]"};
            } else {
                return std::string{"BgAnalogE_{T} [MeV]"};
            }
        } else if (DataName == std::string{"SelectedEt"}) {
            if (MeVtoGeV) {
                return std::string{"Selected E_{T} [GeV]"};
            } else {
                return std::string{"Selected E_{T} [MeV]"};
            }
        } else {
            std::cerr << WarningrMessage << "::DefaultAxisLabel : "
                      << "Unknown Dataname :" << DataName << "\n";
            return std::string{""};
        }
    }
}  // namespace PLISMAnalysis