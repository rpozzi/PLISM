import sys,os
sys.path[0] = os.path.abspath(os.environ['LAR_SIMLATOR_MAIN_DIR'] + '/run/tool')
import Functions as fn
import PlotterModule

#InputFilenames = [
#    os.path.join(fn.RECDIR, 'reconstructed_968435712_athena_mu80_5000000BC_allBC_each50BC_et0-500_tau0.root'),
#]

InputFilenames = ["../OFCcalibration/reconstructed_OFCtest_phase" + str(i) + ".root" for i in range(24)]
InputFilenames = ["../OFCcalibration/reconstructed_OFCtest_phase0.root"]

Plotter = PlotterModule.Plotter(InputFilenames)
#Plotter.TH2DPlotter('SignalTrueEt', 'EtResolution', 50, 0, 50, 100, -0.03, 0.03, True, 'et_TH2D.pdf')

Plotter.SequencePlotter(14700, 19700, "drts", "sequence.pdf", True)
