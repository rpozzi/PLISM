cmake_minimum_required(VERSION 3.15)
set(CMAKE_CXX_FLAGS "-std=c++17 -Wall -Wextra")

project(Plotter)

set(LIBRARY_OUTPUT_PATH ../../test)

add_library(Plotter_lib STATIC 
    src/DefaultLegendGenerator.cpp
    src/ScatterOverlayPlotter.cpp
    src/ScatterPlotter.cpp
    src/SequenceOverlayPlotter.cpp
    src/SequencePlotter.cpp
    src/TGraphPlotterHelper.cpp
    src/TH2DPlotter.cpp
    src/TH2DPlotterHelper.cpp
    src/TrainPeriodicAverageOverlayPlotter.cpp
    src/TrainPeriodicAveragePlotter.cpp
    src/TrainPeriodicBaselineAveragePlotter.cpp
    src/TrainPeriodicBaselineRMSPlotter.cpp
    src/TrainPeriodicRMSOverlayPlotter.cpp
    src/TrainPeriodicRMSPlotter.cpp
)

target_include_directories(Plotter_lib PUBLIC inc PUBLIC ../DataBase/inc PUBLIC ../Generator/inc PUBLIC $ENV{ROOTSYS})

