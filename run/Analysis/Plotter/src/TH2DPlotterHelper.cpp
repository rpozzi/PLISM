#include <TCanvas.h>
#include <TColor.h>
#include <TH2D.h>
#include <TStyle.h>

#include "../../AtlasStyle/inc/AtlasStyle.hpp"
#include "../inc/Plotter.hpp"

namespace PLISMAnalysis
{
    void Plotter::TH2DPlotterHelper(TH2D*& h, const std::string& OutputFilename)
    {
        // canvas
        TCanvas c{"c", "c", 1200, 800};
        gStyle->SetPadTickX(1);
        gStyle->SetPadTickY(1);
        SetAtlasStyle();
        c.SetFrameFillColor(kWhite);

        // color map
        const Int_t NRGBs{5};
        const Int_t NCont{255};
        Double_t stops[NRGBs] = {0.00, 0.34, 0.61, 0.84, 1.00};
        Double_t Red[NRGBs] = {0.00, 0.00, 0.87, 1.00, 0.51};
        Double_t Green[NRGBs] = {0.00, 0.81, 1.00, 0.20, 0.00};
        Double_t Blue[NRGBs] = {0.51, 1.00, 0.12, 0.00, 0.00};
        TColor::CreateGradientColorTable(NRGBs, stops, Red, Green, Blue, NCont);
        gStyle->SetNumberContours(NCont);

        // draw
        h->GetXaxis()->SetNdivisions(505);
        h->GetYaxis()->SetNdivisions(505);
        h->Draw("colz");

        // gPad
        gPad->SetTopMargin(0.2);
        gPad->SetRightMargin(0.2);
        gPad->SetLeftMargin(0.2);
        gPad->SetBottomMargin(0.2);

        // Save
        if (OutputFilename.empty()) {
            c.SaveAs("./figure/TH2D.pdf");
            std::cout << "Outputfile is saved as ./figure/TH2D.pdf"
                      << std::endl;
        } else {
            c.SaveAs(OutputFilename.c_str());
            std::cout << "Outputfile is saved as " << OutputFilename.c_str()
                      << std::endl;
        }
    }
}  // namespace PLISMAnalysis
