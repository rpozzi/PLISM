#include "VecExtractor.h"
#include "./AtlasStyle/AtlasStyle.C"
#include "./AtlasStyle/AtlasUtils.C"
#include "./AtlasStyle/AtlasLabels.C"

#define LEFT -40
#define RIGHT 40
#define TOP 55
#define BOTTOM -15


void ETvstau_multiET() {
  std::unordered_map<unsigned, std::vector<double>> ET10 = getVec("../storage/ReconstructedSequences/reconstructed_OFCtest10.root", "RecEt");
  std::unordered_map<unsigned, std::vector<double>> ET30 = getVec("../storage/ReconstructedSequences/reconstructed_OFCtest30.root", "RecEt");
  std::unordered_map<unsigned, std::vector<double>> ET50 = getVec("../storage/ReconstructedSequences/reconstructed_OFCtest50.root", "RecEt");
  std::unordered_map<unsigned, std::vector<double>> tau10 = getVec("../storage/ReconstructedSequences/reconstructed_OFCtest10.root", "RecTau");
  std::unordered_map<unsigned, std::vector<double>> tau30 = getVec("../storage/ReconstructedSequences/reconstructed_OFCtest30.root", "RecTau");
  std::unordered_map<unsigned, std::vector<double>> tau50 = getVec("../storage/ReconstructedSequences/reconstructed_OFCtest50.root", "RecTau");

  // Set ATLAS Style
  //gStyle->SetPadTickX(1);
  //gStyle->SetPadTickY(1);
  //TCanvas *c = new TCanvas("c", "c",1200, 800);
  //c->Clear();
  //c->Update();
  //SetAtlasStyle();
  //c->SetFrameFillColor(kWhite);

  TString name;
  name.Form("ettau.pdf");
  TCanvas *c1 = new TCanvas(name.Data(), name.Data(), 800, 600);
  c1->Print(name + "[", "pdf");

  int nevent{};
  for (const auto& it : ET10) {

    const auto channelId {it.first};
    const auto et {it.second};
    if (channelId!=960571392)continue;

    TGraph *g10 = new TGraph();
    g10->SetMarkerSize(0.5);
    g10->SetMarkerColor(kBlue);
    g10->SetMarkerStyle(20);
    
    TGraph *g30 = new TGraph();
    g30->SetMarkerSize(0.5);
    g30->SetMarkerColor(kGreen+2);
    g30->SetMarkerStyle(20);

    TGraph *g50 = new TGraph();
    g50->SetMarkerSize(0.5);
    g50->SetMarkerColor(kRed);
    g50->SetMarkerStyle(20);
    
    const int size = et.size()/2;
    for (int i = 0; i < size; ++i) {
      auto e {et[i]};
      if (e) g10->SetPoint(i, tau10[channelId][i], e/1000.);
      e = ET30[channelId][i];
      if (e) g30->SetPoint(i, tau30[channelId][i], e/1000.);
      e = ET50[channelId][i];
      if (e) g50->SetPoint(i, tau50[channelId][i], e/1000.);
      std::cout << i*25 << " " << tau50[channelId][i] << " " << e/1000 << "\n";
    }
    //std::cout << nevent++ << "\n";
    //std::cout << "size: " << size << "*********************\n";
    //TCanvas* c = new TCanvas();
    //c->cd();

    g10->SetTitle(Form("channelId = %d;reconstructed #tau [ns];reconstructed E_{T} [GeV]", static_cast<int>(channelId)));
    g10->GetXaxis()->SetLimits(LEFT, RIGHT);
    g10->SetMaximum(TOP);
    g10->SetMinimum(BOTTOM);
    g50->SetTitle(Form("channelId = %d;reconstructed #tau [ns];reconstructed E_{T} [GeV]", static_cast<int>(channelId)));
    g50->GetXaxis()->SetLimits(LEFT, RIGHT);
    g50->SetMaximum(TOP);
    g50->SetMinimum(BOTTOM);

    //g10->GetXaxis()->SetTitleOffset(0.9);
    //g10->GetYaxis()->SetTitleOffset(0.9);
    //g10->GetXaxis()->SetNdivisions(505);
    //std::cout << g10->GetPointX(1) << " " << g10->GetPointY(1) << "\n";

    //g10->Draw("ap");
    //g30->Draw("p");
    g50->Draw("ap");

    //TLatex *latex1 = new TLatex();
    //latex1->SetTextSize(0.04);
    //latex1->DrawLatexNDC(0.2, 0.75, "Middle layer");
    //latex1->DrawLatexNDC(0.2, 0.7, "#eta = 0.0125, #phi = -1.7211");
    //latex1->DrawLatexNDC(0.2, 0.65, "#mu = 80, true #tau = 0 ns");
    //
    //TLegend *leg = new TLegend(0.2, 0.53, 0.4, 0.63, "");
    //leg->SetName("legend");
    //leg->SetBorderSize(0);
    //leg->SetLineColor(0);
    //leg->SetFillColor(0);
    //leg->SetTextSize(0.035);
    //leg->AddEntry(g10, "true E_{T} = 10 GeV", "pl");
    //leg->AddEntry(g30, "true E_{T} = 30 GeV", "pl");
    //leg->AddEntry(g50, "true E_{T} = 50 GeV", "pl");
    //leg->Draw();
  
    //c->SaveAs(Form("ettau_%d.png", static_cast<int>(channelId)));
    c1->Print(name, "pdf");
  }
  c1->Print(name + "]", "pdf");
}
