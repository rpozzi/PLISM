//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Wed Aug 28 15:23:14 2019 by ROOT version 6.18/00
// from TTree digitSequence/digitSequence
// found on file: digitSequence.root
//////////////////////////////////////////////////////////

#ifndef digitSequence_h
#define digitSequence_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include "deque"

class digitSequence {
public :
  TTree          *fChain;   //!pointer to the analyzed TTree or TChain
  Int_t           fCurrent; //!current Tree number in a TChain

  // Fixed size dimensions of array or collections stored in the TTree if any.

  // Declaration of leaf types
  UInt_t          SCID;
  //deque<int>    *digits;
  Float_t         LSB;
  deque<int>    *digits;

  // List of branches
  TBranch        *b_SCID;   //!
  TBranch        *b_LSB;
  TBranch        *b_digits;   //!

  digitSequence(const char* inputFileName);
  virtual ~digitSequence();
  virtual Int_t    Cut(Long64_t entry);
  virtual Int_t    GetEntry(Long64_t entry);
  virtual Long64_t LoadTree(Long64_t entry);
  virtual void     Init(TTree *tree);
  virtual void     Loop();
  std::deque<int> getDigits();
  virtual Bool_t   Notify();
  virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef digitSequence_cxx
digitSequence::digitSequence(const char* inputFileName) : fChain(0) 
{
  // if parameter tree is not specified (or zero), connect the file
  // used to generate this class and read the Tree.
  TTree *tree = 0;
  if (tree == 0) {
    TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject(inputFileName);
    if (!f || !f->IsOpen()) {
      f = new TFile(inputFileName);
    }
    f->GetObject("digitSequence",tree);

  }
  Init(tree);
}

digitSequence::~digitSequence()
{
  if (!fChain) return;
  delete fChain->GetCurrentFile();
}

Int_t digitSequence::GetEntry(Long64_t entry)
{
  // Read contents of entry.
  if (!fChain) return 0;
  return fChain->GetEntry(entry);
}
Long64_t digitSequence::LoadTree(Long64_t entry)
{
  // Set the environment to read one entry
  if (!fChain) return -5;
  Long64_t centry = fChain->LoadTree(entry);
  if (centry < 0) return centry;
  if (fChain->GetTreeNumber() != fCurrent) {
    fCurrent = fChain->GetTreeNumber();
    Notify();
  }
  return centry;
}

void digitSequence::Init(TTree *tree)
{
  // The Init() function is called when the selector needs to initialize
  // a new tree or chain. Typically here the branch addresses and branch
  // pointers of the tree will be set.
  // It is normally not necessary to make changes to the generated
  // code, but the routine can be extended by the user if needed.
  // Init() will be called many times when running on PROOF
  // (once per file to be processed).

  // Set object pointer
  digits = 0;
  // Set branch addresses and branch pointers
  if (!tree) return;
  fChain = tree;
  fCurrent = -1;
  fChain->SetMakeClass(1);

  fChain->SetBranchAddress("SCID", &SCID, &b_SCID);
  fChain->SetBranchAddress("LSB", &LSB, &b_LSB);
  fChain->SetBranchAddress("digits", &digits, &b_digits);
  Notify();
}

Bool_t digitSequence::Notify()
{
  // The Notify() function is called when a new file is opened. This
  // can be either for a new TTree in a TChain or when when a new TTree
  // is started when using PROOF. It is normally not necessary to make changes
  // to the generated code, but the routine can be extended by the
  // user if needed. The return value is currently not used.

  return kTRUE;
}

void digitSequence::Show(Long64_t entry)
{
  // Print contents of entry.
  // If entry is not specified, print current entry
  if (!fChain) return;
  fChain->Show(entry);
}
Int_t digitSequence::Cut(Long64_t entry)
{
  // This function may be called from Loop.
  // returns  1 if entry is accepted.
  // returns -1 otherwise.
  return 1;
}
#endif // #ifdef digitSequence_cxx
