#ifndef tauresolution_tau_h
#define tauresolution_tau_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include "vector"
#include "deque"

class tauresolution_tau {
public :
  TTree          *fChain;   //!pointer to the analyzed TTree or TChain
  Int_t           fCurrent; //!current Tree number in a TChain

  // Fixed size dimensions of array or collections stored in the TTree if any.

  // Declaration of leaf types
  UInt_t          SCID;
  Float_t          LSB;
  
  vector<double>   *ET_true;
  vector<double>   *tau_true;
  vector<double>   *ETtau_true;
  deque<int>      *digit;
  vector<double>   *ET_of;
  vector<double>   *tau_of;
  vector<double>   *ETtau_of;

  // List of branches
  TBranch        *b_SCID;   //!
  TBranch        *b_LSB;   //!
  TBranch        *b_ET_true;   //!
  TBranch        *b_tau_true;   //!
  TBranch        *b_ETtau_true;   //!
  TBranch        *b_digit;   //!
  TBranch        *b_ET_of;   //!
  TBranch        *b_tau_of;   //!
  TBranch        *b_ETtau_of;   //!

  tauresolution_tau(TTree *tree=0);
  virtual ~tauresolution_tau();
  virtual Int_t    Cut(Long64_t entry);
  virtual Int_t    GetEntry(Long64_t entry);
  virtual Long64_t LoadTree(Long64_t entry);
  virtual void     Init(TTree *tree);
  virtual void     Loop();
  virtual Bool_t   Notify();
  virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef tauresolution_tau_cxx
tauresolution_tau::tauresolution_tau(TTree *tree) : fChain(0) 
{
  // if parameter tree is not specified (or zero), connect the file
  // used to generate this class and read the Tree.
  if (tree == 0) {
    TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("reconstructed_968435712_athena_mu80_50000000BC_allBC_each50BC_et25_tau-24-24.root");
    if (!f || !f->IsOpen()) {
      f = new TFile("reconstructed_968435712_athena_mu80_50000000BC_allBC_each50BC_et25_tau-24-24.root");
    }
    f->GetObject("reconstructed",tree);

  }
  Init(tree);
}

tauresolution_tau::~tauresolution_tau()
{
  if (!fChain) return;
  delete fChain->GetCurrentFile();
}

Int_t tauresolution_tau::GetEntry(Long64_t entry)
{
  // Read contents of entry.
  if (!fChain) return 0;
  return fChain->GetEntry(entry);
}
Long64_t tauresolution_tau::LoadTree(Long64_t entry)
{
  // Set the environment to read one entry
  if (!fChain) return -5;
  Long64_t centry = fChain->LoadTree(entry);
  if (centry < 0) return centry;
  if (fChain->GetTreeNumber() != fCurrent) {
    fCurrent = fChain->GetTreeNumber();
    Notify();
  }
  return centry;
}

void tauresolution_tau::Init(TTree *tree)
{
  // The Init() function is called when the selector needs to initialize
  // a new tree or chain. Typically here the branch addresses and branch
  // pointers of the tree will be set.
  // It is normally not necessary to make changes to the generated
  // code, but the routine can be extended by the user if needed.
  // Init() will be called many times when running on PROOF
  // (once per file to be processed).

  // Set object pointer
  ET_true = 0;
  tau_true = 0;
  digit = 0;
  ET_of = 0;
  tau_of = 0;
  // Set branch addresses and branch pointers
  if (!tree) return;
  fChain = tree;
  fCurrent = -1;
  fChain->SetMakeClass(1);

  fChain->SetBranchAddress("SCID", &SCID, &b_SCID);
  fChain->SetBranchAddress("LSB", &LSB, &b_LSB);
  fChain->SetBranchAddress("ET_true", &ET_true, &b_ET_true);
  fChain->SetBranchAddress("tau_true", &tau_true, &b_tau_true);
  fChain->SetBranchAddress("ETtau_true", &ETtau_true, &b_ETtau_true);
  fChain->SetBranchAddress("digit", &digit, &b_digit);
  fChain->SetBranchAddress("ET_of", &ET_of, &b_ET_of);
  fChain->SetBranchAddress("tau_of", &tau_of, &b_tau_of);
  fChain->SetBranchAddress("ETtau_of", &ETtau_of, &b_ETtau_of);

  fChain->SetBranchStatus("*", 0);
  fChain->SetBranchStatus("SCID", 1);
  //fChain->SetBranchStatus("LSB", 1);
  //fChain->SetBranchStatus("ET_true", 1);
  fChain->SetBranchStatus("tau_true", 1);
  //fChain->SetBranchStatus("ETtau_true", 1);
  //fChain->SetBranchStatus("digit", 1);
  //fChain->SetBranchStatus("ET_of", 1);
  fChain->SetBranchStatus("tau_of", 1);
  //fChain->SetBranchStatus("ETtau_of", 1);

  
  Notify();
}

Bool_t tauresolution_tau::Notify()
{
  // The Notify() function is called when a new file is opened. This
  // can be either for a new TTree in a TChain or when when a new TTree
  // is started when using PROOF. It is normally not necessary to make changes
  // to the generated code, but the routine can be extended by the
  // user if needed. The return value is currently not used.

  return kTRUE;
}

void tauresolution_tau::Show(Long64_t entry)
{
  // Print contents of entry.
  // If entry is not specified, print current entry
  if (!fChain) return;
  fChain->Show(entry);
}
Int_t tauresolution_tau::Cut(Long64_t entry)
{
  // This function may be called from Loop.
  // returns  1 if entry is accepted.
  // returns -1 otherwise.
  return 1;
}
#endif // #ifdef tauresolution_tau_cxx
