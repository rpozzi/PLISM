import sys,os
sys.path[0] = os.path.abspath("tool")
import Functions as fn
import CellPropertyGeneratorModule


PulseShapeFileName = os.path.join(fn.PULSESHAPEDIR, 'PulseShapes.root')
NoiseFileName = os.path.join(fn.NOISEDIR, 'CellNoiseDB.root')
OutputFileName = os.path.join(fn.CELLPROPDIR, 'CellProperty.root')

CellPropertyGenerator = CellPropertyGeneratorModule.CellPropertyGenerator()
CellPropertyGenerator.generateFromAREUSPulseShapeTree(PulseShapeFileName, NoiseFileName, OutputFileName)

'''
PulsePattern = CellPropertyGeneratorModule.PulsePattern(
    '/eos/atlas/unpledged/group-tokyo/users/mfurukaw/public/data4PLISM/DigitAnalysis_material/HighSCDelay_FMhighEtaBack/parameters.dat',
    '/eos/atlas/unpledged/group-tokyo/users/mfurukaw/public/data4PLISM/DigitAnalysis_material/LArIDtranslator_I12LI13R_LArcellID_CL.txt'
) 
PulsePattern.outputPattern()
                                                            
DigitTree = CellPropertyGeneratorModule.DigitTree(
    '/eos/atlas/unpledged/group-tokyo/users/mfurukaw/public/data4PLISM/DigitAnalysis_material/LArDigits_382578.root',
    PulsePattern,          
    '/eos/atlas/unpledged/group-tokyo/users/mfurukaw/public/data4PLISM/DigitAnalysis_material/LArIDtranslator_I12LI13R_SCID_CL.txt',
    '/eos/atlas/unpledged/group-tokyo/users/mfurukaw/public/data4PLISM/DigitAnalysis_material/LArPedAutoCorr_00382577.root',
    '/eos/atlas/unpledged/group-tokyo/users/mfurukaw/public/data4PLISM/DigitAnalysis_material/LArIDtranslator_I12LI13R_LArcellID_SCID.txt'
)
DigitTree.MakeTree()

AveragedDigitTree = CellPropertyGeneratorModule.AveragedDigitTree(
    'LArDigits_382578_converted.root'
)
AveragedDigitTree.PulseShape();
'''
