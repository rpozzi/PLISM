import sys,os
sys.path[0] = os.path.abspath(os.environ['LAR_SIMLATOR_MAIN_DIR'] + '/run/tool')
import Functions as fn
import CellPropertyGeneratorModule


LArIDtranslatorList = "LArIDtranslator_test.txt"

target = ["ONL_ID", "SC_ONL_ID", "CL"]
#condition = "FTNAME like 'I01L' or FTNAME like 'I05R' or FTNAME like 'I02L' or FTNAME like 'I02R' or FTNAME like 'I03L' or FTNAME like 'I03R' or FTNAME like 'I04L' or FTNAME like 'I04R' or FTNAME like 'A%' or FTNAME like 'C%'"
condition = "(FTNAME like 'I%' or FTNAME like 'A%' or FTNAME like 'C%') and (DET = 0) or (DET = 1)"
#condition = "FTNAME like 'I03L'"
overlap_canceller = True
fn.MakeListFromLArIDtranslator(target, condition, LArIDtranslatorList, overlap_canceller)


CaliWaveTree = CellPropertyGeneratorModule.CaliWaveTree(
    #'/eos/atlas/unpledged/group-tokyo/users/mfurukaw/public/data4PLISM/LArCaliWave_I03L_sat_merged_revised_4SCs.root'
    'LArCaliWave_merged.root'
)
CaliWaveTree.ID_SCID_CL_fileName = LArIDtranslatorList
CaliWaveTree.DAC2MeVFileName = 'LArDAC2MeV.root'
CaliWaveTree.LSB_delayPeak()
#CaliWaveTree.PedFileName = 'LArPedAutoCorr_00389320.root'
CaliWaveTree.PedFileName = 'LArPedAutoCorr_merged.root'
CaliWaveTree.PulseTree()
LSBTree = CellPropertyGeneratorModule.LSBTree(
    'LArCaliWave_LSBDelayPeak.root'
)
#1 Detector ('EMB' or 'EMEC')
#2 LSB reference file
#3 Upper limit of LSB in output graph (max value is set as upper limit if you set 0)
#LSBTree.LSBvsEta('EMB', os.path.join(fn.CELLPROPDIR, 'LSBreference.txt'), 600)

PulseShapeTree = CellPropertyGeneratorModule.PulseShapeTree(
    'LArCaliWave_pulse.root'
)
PulseShapeTree.PedAlreadySubtracted = True
PulseShapeTree.GenerateCellProperty(
    'LArCaliWave_LSBDelayPeak.root',
    os.path.join(fn.CELLPROPDIR, 'CellProperty_test.root'),
    0
)
#PulseShapeTree.DrawPulseShape()
