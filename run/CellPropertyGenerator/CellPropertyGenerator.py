import sys,os
sys.path[0] = os.path.abspath(os.environ['LAR_SIMLATOR_MAIN_DIR'] + '/run/tool')
import Functions as fn
import CellPropertyGeneratorModule

target = ["ONL_ID", "SC_ONL_ID", "CL"]
#condition = "FTNAME like 'I01L' or FTNAME like 'I05R' or FTNAME like 'I02L' or FTNAME like 'I02R' or FTNAME like 'I03L' or FTNAME like 'I03R' or FTNAME like 'I04L' or FTNAME like 'I04R'"
#condition = "FTNAME like 'I05L' or FTNAME like 'I06L' or FTNAME like 'I06R' or FTNAME like 'I07L' or FTNAME like 'I07R' or FTNAME like 'I08L' or FTNAME like 'I08R' or FTNAME like 'I09R'"
condition = "FTNAME like 'I%'"
LArIDtranslatorList = "LArIDtranslator_test.txt"
overlap_canceller = True
fn.MakeListFromLArIDtranslator(target, condition, LArIDtranslatorList, overlap_canceller)

CellPropertyGenerator = CellPropertyGeneratorModule.CellPropertyGenerator()
CellPropertyGenerator.PatternFileName = 'parameters.dat'
CellPropertyGenerator.ID_SCID_CL_FileName = LArIDtranslatorList
CellPropertyGenerator.DigitFileName = 'LArDigits_382579.root'
CellPropertyGenerator.PedestalFileName = 'LArPedAutoCorr_00382577.root'
CellPropertyGenerator.DAC2MeVFileName = 'LArDAC2MeV.root'
CellPropertyGenerator.NTriggers = 100
CellPropertyGenerator.NSamples = 32
CellPropertyGenerator.Detector = 'EMB'
CellPropertyGenerator.LSBReferenceFileName = os.path.join(fn.CELLPROPDIR, 'LSBreference.txt')
CellPropertyGenerator.LSBPlotUpperLimit = 600
CellPropertyGenerator.OutputCellPropertyFileName = 'CellProperty_test.root'
CellPropertyGenerator.BCShift = 0
CellPropertyGenerator.GenerateFromCalibFrameWork()
