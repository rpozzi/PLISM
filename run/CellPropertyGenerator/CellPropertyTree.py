import sys,os
sys.path[0] = os.path.abspath(os.environ['LAR_SIMLATOR_MAIN_DIR'] + '/run/tool')
import Functions as fn
import CellPropertyGeneratorModule

CellPropertyTree = CellPropertyGeneratorModule.CellPropertyTree(
    os.path.join(fn.CELLPROPDIR, 'CellProperty_test.root')
)
CellPropertyTree.PlotPedestalADC(1100)
CellPropertyTree.PlotPedestalGeV(650)
CellPropertyTree.PlotRMSADC(1.2)
CellPropertyTree.PlotRMSMeV(450)
