import sys,os
sys.path[0] = os.path.abspath(os.environ['LAR_SIMLATOR_MAIN_DIR'] + '/run/tool')
import Functions as fn
import CellPropertyGeneratorModule

PulsePattern = CellPropertyGeneratorModule.PulsePattern(
    '/eos/atlas/unpledged/group-tokyo/users/mfurukaw/public/data4PLISM/DigitAnalysis_material/HighSCSaturation_FMhighEtaBack_200908_8DACs/parameters.dat',
    'LArIDtranslator_test.txt'
)
PulsePattern.outputPattern()

DigitTree = CellPropertyGeneratorModule.DigitTree(
    '/eos/atlas/unpledged/group-tokyo/users/mfurukaw/public/data4PLISM/DigitAnalysis_material/LArDigits_382579.root',
    PulsePattern,
    'LArIDtranslator_test.txt',
    '/eos/atlas/unpledged/group-tokyo/users/mfurukaw/public/data4PLISM/DigitAnalysis_material/LArPedAutoCorr_00382577.root',
    '/eos/atlas/unpledged/group-tokyo/users/mfurukaw/public/data4PLISM/LArDAC2MeV.root',
    100,
    32
)
DigitTree.MakeTree()
