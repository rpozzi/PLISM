import sys,os
sys.path[0] = os.path.abspath(os.environ['LAR_SIMLATOR_MAIN_DIR'] + '/run/tool')
import Functions as fn
import CellPropertyGeneratorModule

LSBTree = CellPropertyGeneratorModule.LSBTree(
    'LArCaliWave_LSBDelayPeak.root'
)

#1 Detector ('EMB' or 'EMEC')
#2 LSB reference file
#3 Upper limit of LSB in output graph (max value is set as upper limit if you set 0)
#LSBTree.LSBvsEta('EMB', os.path.join(fn.CELLPROPDIR, 'LSBreference.txt'), 600)
LSBTree.LSBvsEta('', os.path.join(fn.CELLPROPDIR, 'LSBreference.txt'), 600)
