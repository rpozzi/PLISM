void CellSelector(const char* inputFileName, const char* outputFileName) {
  TFile *fin = new TFile(inputFileName, "read");
  TTree *tin = (TTree*)fin->Get("CellProperty");
  if (tin == 0) tin = (TTree*)fin->Get("HitSummary");
  TFile* ofile = new TFile(outputFileName, "RECREATE");
  
  TTree* newtree = tin->CopyTree("channelId == 7905");

  ofile->cd();
  newtree->Write();
  ofile->Close();
}
