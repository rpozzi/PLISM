import sys,os
import ROOT
sys.path[0] = os.path.abspath(os.environ['LAR_SIMLATOR_MAIN_DIR'] + '/run/tool')
import Functions as fn
ROOT.gROOT.SetBatch(True)
import ReconstructorModule

Reconstructor = ReconstructorModule.Reconstructor('files/digitSequence.root')
Reconstructor.OutputFileName = 'files/reconstructed.root'
Reconstructor.OFCFileName = 'files/OFC_968435712_athena_mu80.root'
Reconstructor.OutputBranch_PileupTrueEt = False
Reconstructor.OutputBranch_PileupTrueTau = False
Reconstructor.OutputBranch_PileupTrueEtSum = False
Reconstructor.reconstruct()
