import sys,os
sys.path[0] = os.path.abspath(os.environ['LAR_SIMLATOR_MAIN_DIR'] + '/run/tool')
import Functions as fn
import CellPropertyGeneratorModule

DirectGenerator = CellPropertyGeneratorModule.DirectGenerator()
DirectGenerator.PatternFileName = 'files/parameters_SCDelay_FMhighEtaBack_389323_210304-195651.dat'
DirectGenerator.ID_SCID_CL_FileName = 'files/LArIDtranslator_test.txt'
DirectGenerator.DigitFileName = 'files/LArDigits_00389323.root'
DirectGenerator.PedestalFileName = 'files/LArPedAutoCorr_00389320.root'
DirectGenerator.DAC2MeVFileName = 'files/LArDAC2MeV.root'
DirectGenerator.NTriggers = 100
DirectGenerator.NSamples = 32
DirectGenerator.Detector = 'EMB'
DirectGenerator.LSBReferenceFileName = 'files/LSBreference.txt'
DirectGenerator.LSBPlotUpperLimit = 600
DirectGenerator.BCShift = 3
DirectGenerator.GenerateFromCalibFrameWork()
