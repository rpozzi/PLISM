import sys,os
import ROOT as root
sys.path[0] = os.path.abspath(os.environ['LAR_SIMLATOR_MAIN_DIR'] + '/run/tool')
import Functions as fn
root.gROOT.SetBatch(True)
import CellPropertyGeneratorModule

CaliWaveTree = CellPropertyGeneratorModule.CaliWaveTree(
    'files/LArCaliWave_I03L_sat_merged_revised_4SCs.root'
)
CaliWaveTree.ID_SCID_CL_fileName = 'files/LArIDtranslator_test.txt'
CaliWaveTree.DAC2MeVFileName = 'files/LArDAC2MeV.root'
CaliWaveTree.LSB_delayPeak()

CaliWaveTree.PedFileName = 'files/LArPedAutoCorr_00390803.root'
CaliWaveTree.PulseTree()

PulseShapeTree = CellPropertyGeneratorModule.PulseShapeTree(
    'files/pulse_LArCaliWave_I03L_sat_merged_revised_4SCs.root'
)
PulseShapeTree.PedAlreadySubtracted = True
PulseShapeTree.LSBFileName = 'files/lsb_LArCaliWave_I03L_sat_merged_revised_4SCs.root'
PulseShapeTree.BCShift = 0
PulseShapeTree.GenerateCellProperty()
