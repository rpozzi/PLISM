import ROOT as root
import atlasplots as aplt
import math, glob
root.gROOT.SetBatch(True)
aplt.set_atlas_style()

ifnames = glob.glob("files/ramp_*.txt")
chids = [x.split("ramp_")[-1].split(".txt")[0] for x in ifnames]

fout = root.TFile('canvas.root', "recreate")

for ifname, chid in zip(ifnames, chids):
    gObs = root.TGraph()
    gSmooth = root.TGraph()

    fin = open(ifname, "r")
    smooth = False
    for line in fin:
        line = line.strip()
        if not line:
            smooth = True
            continue
        v = [float(x) for x in line.split()]
        if smooth:
            gSmooth.SetPoint(gSmooth.GetN(), v[0]*1e-3, v[1])
        else:
            gObs.SetPoint(gObs.GetN(), v[0]*1e-3, v[1])
    fin.close()

    fig, ax = aplt.subplots(1, 1)
    ax.plot(gSmooth, options="l")
    ax.plot(gObs, options="p")
    ax.set_xlabel("E_{T} [GeV]")
    ax.set_ylabel("ADC")
    ax.add_margins(top=0.05)
    fig.canvas.SetName(chid)
    fig.canvas.Write()
