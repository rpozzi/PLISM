import ROOT as root
import atlasplots as aplt
import math
root.gROOT.SetBatch(True)
aplt.set_atlas_style()

fin = root.TFile('files/reconstructed_Sequence_AllEt_Tau0_phase0.root', "read")
tin = fin.Get("reconstructed")
fout = root.TFile('canvas.root', "recreate")
nEntries = tin.GetEntries()

gm2 = root.TGraph()
gm1 = root.TGraph()
g0 = root.TGraph()
gp1 = root.TGraph()

nEntries = tin.GetEntries()
for j in range(nEntries):
    tin.GetEntry(j)

    for i in range(2, len(tin.RecEt)-1):
        if not tin.SignalTrueEt[i]:
            continue
        
        gm2.SetPoint(i, tin.RecEt[i-2], tin.RecEtTau[i-2])
        gm1.SetPoint(i, tin.RecEt[i-1], tin.RecEtTau[i-1])
        g0.SetPoint(i, tin.RecEt[i], tin.RecEtTau[i])
        gp1.SetPoint(i, tin.RecEt[i+1], tin.RecEtTau[i+1])

    fig, ax = aplt.subplots(1, 1)
    fig.canvas.SetCanvasSize(800, 600)
    ax.plot(gm2, options="p", markercolor=root.kAzure-6, markersize=0.5)
    ax.plot(gm1, options="p", markercolor=root.kOrange+10, markersize=0.5)
    ax.plot(g0, options="p", markercolor=root.kBlack, markersize=0.5)
    ax.plot(gp1, options="p", markercolor=root.kTeal-6, markersize=0.5)
    ax.set_xlabel("E_{T} [MeV]")
    ax.set_ylabel("E_{T}#tau [MeV#upointns]")
    ax.add_margins(top=0.05, bottom=0.05, right=0.05)

    if tin.layer==0:
        layerStr = "Presampler"
    elif tin.layer==1:
        layerStr = "Front layer"
    elif tin.layer==2:
        layerStr = "Middle layer"
    elif tin.layer==3:
        layerStr = "Back layer"
    ax.text(0.2, 0.86, layerStr, size=22, align=13)
    ax.text(0.2, 0.82, "#eta =  " + "{:.4f}".format(tin.eta) + ", #phi = " + "{:.4f}".format(tin.phi), size=22, align=13)

    fig.canvas.SetName(str(tin.channelId))
    fig.canvas.Write()
    
