import sys,os
sys.path[0] = os.path.abspath(os.environ['LAR_SIMLATOR_MAIN_DIR'] + '/run/tool')
import Functions as fn

LArIDtranslatorList = "files/LArIDtranslator_test.txt"
target = ["ONL_ID", "SC_ONL_ID", "CL"]
condition = "FTNAME like 'A01L'"
overlap_canceller = True
fn.MakeListFromLArIDtranslator(target, condition, LArIDtranslatorList, overlap_canceller)
