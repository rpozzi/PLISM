import sys,os
import ROOT as root
sys.path[0] = os.path.abspath(os.environ['LAR_SIMLATOR_MAIN_DIR'] + '/run/tool')
import Functions as fn
root.gROOT.SetBatch(True)
import CellPropertyGeneratorModule

LSBTree = CellPropertyGeneratorModule.LSBTree(
    'files/CaliWave_LSBDelayPeak.root'
)
LSBTree.OutputLSBphysName = 'files/LSBphys_test.root'
LSBTree.LSBphys('files/MPMC_test.root')
#1 Detector ('EMB' or 'EMEC')
#2 LSB reference file
#3 Upper limit of LSB in output graph (max value is set as upper limit if you set 0)
#LSBTree.LSBvsEta('EMB', os.path.join(fn.CELLPROPDIR, 'LSBreference.txt'), 600)

PhysWaveTree = CellPropertyGeneratorModule.PhysWaveTree(
    'files/PhysWave_test.root'
    )
PhysWaveTree.ID_SCID_CL_fileName = 'files/LArIDtranslator_test.txt'
PhysWaveTree.DAC2MeVFileName = 'files/LArDAC2MeV.root'
PhysWaveTree.PedFileName = 'files/LArPedAutoCorr_20211020_v1.root'
PhysWaveTree.InputMPMCFileName = 'files/MPMC_test.root'
PhysWaveTree.PulseTree('files/CaliWave_LSBDelayPeak.root')

PulseShapeTree = CellPropertyGeneratorModule.PulseShapeTree(
    'files/pulse_PhysWave_test.root'
)
PulseShapeTree.PedAlreadySubtracted = True
PulseShapeTree.LSBFileName = 'files/LSBphys_test.root'
PulseShapeTree.BCShift = 0
PulseShapeTree.GenerateCellProperty()
