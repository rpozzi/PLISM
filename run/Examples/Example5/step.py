import sys,os
import ROOT as root
sys.path[0] = os.path.abspath(os.environ['LAR_SIMLATOR_MAIN_DIR'] + '/run/tool')
import Functions as fn
root.gROOT.SetBatch(True)
import SCconditionSimulatorModule

SCconditionSimulator = SCconditionSimulatorModule.ReconstructedTree('files/reconstructedCali_EMEC0_OFCphase23.root')
SCconditionSimulator.OutputFileName = 'files/RecoAccuracyTree.root'
SCconditionSimulator.OutputBranch_ReldiffRecTrueEt = False
SCconditionSimulator.OutputBranch_MeanReldiffRecTrueEt = False
SCconditionSimulator.OutputBranch_RMSReldiffRecTrueEt = False
SCconditionSimulator.inputCellpropFileName = 'files/cellprop_CaliWave_EMEC0.root'
SCconditionSimulator.RecoAccuracyTree() 


