import sys,os
import subprocess
sys.path[0] = os.path.abspath(os.environ['LAR_SIMLATOR_MAIN_DIR'] + '/run/tool')
import Functions as fn
import HitSummarizerModule

print("Making HitSummaryLowPt.root ...")
HitSummarizerLow = HitSummarizerModule.HitSummarizer('HITSforAREUS.05608147._000001.root')
HitSummarizerLow.OutputFileName = os.path.join(fn.HITDIR, 'HitSummaryLowPt.root')
HitSummarizerLow.LITFileName = 'LArIDtranslator_test.root'
HitSummarizerLow.summarize()

print("Making HitSummaryHighPt.root ...")
HitSummarizerHigh = HitSummarizerModule.HitSummarizer('HITSforAREUS.05608152._000001.root')
HitSummarizerHigh.OutputFileName = os.path.join(fn.HITDIR, 'HitSummaryHighPt.root')
HitSummarizerHigh.LITFileName = 'LArIDtranslator_test.root'
HitSummarizerHigh.summarize()
