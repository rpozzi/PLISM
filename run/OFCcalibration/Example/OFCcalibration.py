import sys,os
sys.path[0] = os.path.abspath(os.environ['LAR_SIMLATOR_MAIN_DIR'] + '/run/tool')
import Functions as fn
import OFCCalibratorModule

NthPhase = 23
withNoise = True
withPileup = False

CellPropertyFileName = os.path.join(fn.CELLPROPDIR, 'CellProperty_test.root')
OFCFileName = os.path.join(fn.OFCDIR, 'OFC_test_cali_phase' + str(NthPhase) + '.root')
OFCCalibrator = OFCCalibratorModule.OFCCalibrator(CellPropertyFileName)
OFCCalibrator.NBC = 10000
OFCCalibrator.mu = 0
OFCCalibrator.phase = NthPhase * 1.04
OFCCalibrator.TrainPattern = [0, 1]
OFCCalibrator.OutputFileName = OFCFileName
OFCCalibrator.LowPtPileupHitSummaryFile = "/eos/atlas/unpledged/group-tokyo/users/mfurukaw/data4PLISM/HitSummary_iguchiLow_968435712_channelId.root"
OFCCalibrator.HighPtPileupHitSummaryFile = "/eos/atlas/unpledged/group-tokyo/users/mfurukaw/data4PLISM/HitSummary_iguchiHigh_968435712_channelId.root"
OFCCalibrator.seed = 12345
OFCCalibrator.BCID0thSamp = 3406
OFCCalibrator.calibrate(withNoise, withPileup)
print('Phase ' + str(NthPhase * 1.04) + ' done')
