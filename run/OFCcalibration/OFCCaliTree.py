import sys,os
sys.path[0] = os.path.abspath(os.environ['LAR_SIMLATOR_MAIN_DIR'] + '/run/tool')
import Functions as fn
import OFCCalibratorModule

args = sys.argv
NthPhase = int(args[1])

OFCCaliTree = OFCCalibratorModule.OFCCaliTree('LArOFCCali_00389323.root')
OFCCaliTree.NthPhase = NthPhase
OFCCaliTree.OFBCshift = 0
OFCCaliTree.OutputFileName = os.path.join(fn.OFCDIR, 'OFC_test_cali_phase' + str(NthPhase) + '.root')
OFCCaliTree.CellPropertyFileName = os.path.join(fn.CELLPROPDIR, 'CellProperty_958474240.root')
OFCCaliTree.MakeTree()
