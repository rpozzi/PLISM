import sys,os
sys.path[0] = os.path.abspath("tool")
import Functions as fn
import HitSummarizerModule
import DigitizerModule
import ReconstructorModule

args = sys.argv
phase = int(args[1])

CellPropertyFileName = os.path.join(fn.CELLPROPDIR, 'CellProperty_968435712.root')
SignalHitSummaryFile = os.path.join(fn.HITDIR, 'HitSummaryTest.root')
HitSummarizer = HitSummarizerModule.HitSummarizer()
HitSummarizer.OutputFileName = SignalHitSummaryFile
HitSummarizer.CellPropertyFileName = CellPropertyFileName
HitSummarizer.NEvent = 500000
# Uniform distribution
HitSummarizer.EtMin = 0 #MeV
HitSummarizer.EtMax = 50000 #MeV
HitSummarizer.TauMin = 0 #ns
HitSummarizer.TauMax = 0 #ns
HitSummarizer.makeSignal()

DigitSequenceFileName = os.path.join(fn.DIGITDIR, 'digitSequence_968435712_mu50_phase' + str(phase * 1.04) + '.root')
Digitizer = DigitizerModule.Digitizer(CellPropertyFileName)
Digitizer.NBC = 10000000
Digitizer.mu = 50
Digitizer.phase = 0
Digitizer.TrainPattern = [1, 0]
Digitizer.SignalPattern = [1, 49]
Digitizer.OutputFileName = DigitSequenceFileName
Digitizer.SignalHitSummaryFile = SignalHitSummaryFile
Digitizer.LowPtPileupHitSummaryFile = os.path.join(fn.HITDIR, 'HitSummary_iguchiLow_968435712.root')
Digitizer.HighPtPileupHitSummaryFile = os.path.join(fn.HITDIR, 'HitSummary_iguchiHigh_968435712.root')
Digitizer.sequence()

Reconstructor = ReconstructorModule.Reconstructor(DigitSequenceFileName)
Reconstructor.OutputFileName = os.path.join(fn.RECDIR, 'reconstructed_968435712_mu50_phase' + str(phase * 1.04) + '.root')
Reconstructor.OFCFileName = os.path.join(fn.OFCDIR, 'OFC_968435712_mu50_phase' + str(phase * 1.04) + '.root')
Reconstructor.reconstruct()
