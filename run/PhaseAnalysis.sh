#!/bin/sh

for phase in `seq 0 23`
do
    python OFCcalibration.py $phase
    python PhaseAnalysis.py $phase
done
