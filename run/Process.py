import sys,os
sys.path[0] = os.path.abspath("tool")
import Functions as fn
import HitSummarizerModule
import DigitizerModule
import ReconstructorModule

CellPropertyFileName = os.path.join(fn.CELLPROPDIR, 'CellProperty_968435712.root')

SignalHitSummaryFile = os.path.join(fn.HITDIR, 'HitSummaryTest.root')
HitSummarizer = HitSummarizerModule.HitSummarizer()
HitSummarizer.OutputFileName = SignalHitSummaryFile
HitSummarizer.CellPropertyFileName = CellPropertyFileName
HitSummarizer.NEvent = 1000000
# Uniform distribution
HitSummarizer.EtMin = 0 #MeV
HitSummarizer.EtMax = 500000 #MeV
HitSummarizer.TauMin = 0 #ns
HitSummarizer.TauMax = 0 #ns
HitSummarizer.makeSignal()

DigitSequenceFileName = os.path.join(fn.DIGITDIR, 'digitSequence_968435712_mu80_5000000BC_allBC_each50BC_et0-500_tau0.root')
Digitizer = DigitizerModule.Digitizer()
Digitizer.CellPropertyFileName = CellPropertyFileName
Digitizer.NBC = 5000000
Digitizer.mu = 80
Digitizer.phase = 0
Digitizer.TrainPattern = [1, 0]
Digitizer.SignalPattern = [1, 49]
Digitizer.OutputFileName = DigitSequenceFileName
Digitizer.SignalHitSummaryFile = SignalHitSummaryFile
Digitizer.LowPtPileupHitSummaryFile = '/eos/atlas/unpledged/group-tokyo/users/mfurukaw/public/data4PLISM/HitSummary_LowPt_968435712.root'
Digitizer.HighPtPileupHitSummaryFile = '/eos/atlas/unpledged/group-tokyo/users/mfurukaw/public/data4PLISM/HitSummary_HighPt_968435712.root'
Digitizer.OutputBranch_PileupTrueEt = True
Digitizer.OutputBranch_PileupTrueTau = True
Digitizer.OutputBranch_PileupTrueEtSum = True
Digitizer.sequence()

Reconstructor = ReconstructorModule.Reconstructor(DigitSequenceFileName)
Reconstructor.OutputFileName = os.path.join(fn.RECDIR, 'reconstructed_968435712_athena_mu80_5000000BC_allBC_each50BC_et0-500_tau0.root')
Reconstructor.OFCFileName = os.path.join(fn.OFCDIR, 'OFC_968435712_athena_mu80.root')
Reconstructor.OutputBranch_PileupTrueEt = True
Reconstructor.OutputBranch_PileupTrueTau = True
Reconstructor.OutputBranch_PileupTrueEtSum = True
Reconstructor.reconstruct()
