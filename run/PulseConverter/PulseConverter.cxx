#include "PulseConverter.h"

#define PhysicsPulseName "physwave_data285005_f17ch7.dat"

const double PulseConverter::delta {25./24};

PulseConverter::PulseConverter(const char* CalibrationPulseName) {
  std::ifstream fin(CalibrationPulseName);
  double d1, d2;
  while (fin >> d1 >> d2) g_cali.push_back(d2);
  fin.close();

  const auto gbegin {g_cali.begin()};
  const auto gend   {g_cali.end()};
  const double maxAmp {*std::max_element(gbegin, gend)};
  for (auto&& it : g_cali) it /= maxAmp;

  size = g_cali.size();
  tMaxAmp = std::max_element(gbegin, gend) - gbegin;
  tMinAmp = std::min_element(gbegin, gend) - gbegin;
  for (int i{tMaxAmp+1}; i < size; ++i) {
    if (g_cali[i] < 0) {
      tZeroCross = i;
      break;
    }
  }
}

void PulseConverter::ComputePhysicsPulse() {
  constexpr std::array<double, 4> TdriftArray {{420., 469., 469., 469.}};
  const double Tdrift {TdriftArray[layer]};
  const double ftinv {1./(f_step * Tdrift)};
  auto eff = [this](double t) -> double {return (1. - f_step) * exp(-f_step * t / tau_cali);};
  
  std::vector<double> gMB(size);
  std::vector<double> gExp(size);
  std::function<double(const double)> func {InjectPointCorrectionFunc(tau_0, tau_r)};
  for (int i{}; i < size; ++i) {
    const double t {i * delta};
    gMB[i] = func(t);
    gExp[i] = (i ? 0 : 1.) + eff(t) * (1./tau_cali + ftinv) - ftinv - ((t >= Tdrift) ? ((eff(t - Tdrift) - 1.) * ftinv) : 0);
  }
  g_phys = convolution(g_cali, convolution(gMB, gExp));

  ofstream fout(PhysicsPulseName);
  for (int i{}; i < size; ++i) fout << i << " " << g_phys[i] << "\n";  
  fout.close();
  
  DrawGraph();
  PlotGWaves("gwaves");
}

void PulseConverter::DrawGraph() {
  std::unique_ptr<TCanvas> c = std::make_unique<TCanvas>("c", "", 800, 600);
  c->cd();

  std::unique_ptr<TGraph> g = std::make_unique<TGraph>(PhysicsPulseName);
  g->SetMarkerStyle(21);
  g->SetMarkerSize(0.3);
  
  g->Draw("ap");
  c->SaveAs("test.pdf");
}

void PulseConverter::ComputeParameters() {
  TauCaliExtraction();
  StepResponseCalculation();
  CosineResponseCalculation();
  InjectPointCalculation();
  
  cout << "tau_cali = " << tau_cali << "\n"
       << "f_step = " << f_step << "\n"
       << "tau_0 = " << tau_0 << "\n"
       << "tau_r = " << tau_r << "\n";
}

void PulseConverter::TauCaliExtraction() {
  const int t1{tMinAmp + 150}, t2{size - 125};
  const int length {t2 - t1 + 1};
  double Sx{}, Sy{}, Sx2{}, Sxy{};
  for (int i{t1}; i <= t2; ++i) {
    const double x {i * delta};
    const double logy {log(fabs(g_cali[i]))};
    Sx += x;
    Sy += logy;
    Sx2 += x * x;
    Sxy += x * logy;
  }
  tau_cali = (Sx * Sx - length * Sx2) / (length * Sxy - Sx * Sy);
  cout << "tau_cali = " << tau_cali << "\n";  
}

void PulseConverter::StepResponseCalculation() {
  const int tail{tMinAmp + 100};
  f_step = 0.;
  auto ftran = [this](const double f_step) -> std::vector<double> {
    std::vector<double> v(size);
    const double A {(1. - f_step) / tau_cali};
    const double B {-f_step * delta / tau_cali};
    for (int i{}; i < size; ++i) v[i] = (i ? 0 : 1) + A * exp(i * B);
    return v;
  };
  auto dftran = [this](const double f_step) -> std::vector<double> { // d(ftran)/d(f_step)
    std::vector<double> v(size);
    const double A {-delta * f_step / tau_cali};
    const double B {delta * (f_step - 1.) / (tau_cali * tau_cali)};
    const double C {1. / tau_cali};
    for (int i{}; i < size; ++i) v[i] = exp(i * A) * (i * B - C);
    return v;
  };
  auto dFstep = [&](const double f_step) -> double {
    const std::vector<double> w {w_out(ftran(f_step), tail)};
    const std::vector<double> dw {w_out(dftran(f_step), tail)};
    return -std::inner_product(w.begin(), w.end(), dw.begin(), 0.) / std::inner_product(dw.begin(), dw.end(), dw.begin(), 0.);
  };
  for (int niter{1};; ++niter) {
    const double dfstep {dFstep(f_step)};
    f_step += dfstep;
    if (niter > 50 || f_step < 0.) {
      f_step = 0.;
      break;
    }
    if (fabs(dfstep) <= 2.22045e-16) break;
  }
  cout << "f_step = " << f_step << "\n";
}

void PulseConverter::CosineResponseCalculation() {
  constexpr double tau_sh {15.};
  constexpr std::array<double, 4> omegaminArray {{.1, .22, .06, .1}};
  constexpr std::array<double, 4> omegamaxArray {{.6, .31, .215, .7}};
  constexpr std::array<int, 4> npoints {{800, 900, 310, 120}};
  const int tail{tMinAmp + 50};
  const double omega0min{omegaminArray[layer]}, omega0max{omegamaxArray[layer]};
  auto ftran = [this](const double omega) -> std::vector<double> {
    const double C1 {(f_step * f_step - f_step * f_step * f_step) / tau_cali};
    const double C2 {(f_step + omega * omega * tau_cali * tau_cali)};
    const double C3 {(f_step - 1.) * omega * tau_cali};
    const double C4 {(f_step * f_step + omega * omega * tau_cali * tau_cali )};
    std::vector<double> v(size);
    for (int i{}; i < size; ++i) {
      const double t {i * delta};
      v[i] = (i ? 0 : 1) + (C1 * exp(-f_step * t / tau_cali) - omega * (C2 * sin(omega * t) + C3 * cos(omega * t))) / C4;
    }
    return v;
  };
  std::function<double(double)> logQ2_omega0 = [&](const double omega) -> double {
    std::vector<double> w {w_out(ftran(omega), tail)};
    const double correction {pow((1 + omega * omega * tau_sh * tau_sh), 1.5) / (omega * tau_sh)};
    for (auto&& it : w) it *= correction;
    return log(std::inner_product(w.begin(), w.end(), w.begin(), 0.));
  };
  Plot1DFunctionGraph(logQ2_omega0, omega0min, omega0max, "logQ2_omega0");
  const double omega {FMINBRsolution(logQ2_omega0, omega0min, omega0max)};
  tau_0 = 1. / omega;
  cout << "omega_0 = " << omega << ", tau_0 = " << tau_0 << "\n";
}

void PulseConverter::InjectPointCalculation() {
  const int tail{tMinAmp};
  const double TAUR_A{0.}, TAUR_B{100.};
  auto ftran = [this](const double tau_r) -> std::vector<double> {
    std::function<double(const double)> func = InjectPointCorrectionFunc(tau_0, tau_r);
    std::vector<double> v(size);
    for (int i{}; i < size; ++i) v[i] = func(i * delta);
    return v;
  };
  auto Q2_taur = [&](const double tau_r) -> double {
    std::vector<double> wold {w_out(ftran(tau_r), tail)};
    const int wsize {size - tail};
    std::vector<double> wnew(wsize);
    for (int i{}; i < wsize; ++i) wnew[i] = g_cali[tail + i] - wold[i];
    return std::inner_product(wnew.begin(), wnew.end(), wnew.begin(), 0.);
  };
  Plot1DFunctionGraph(Q2_taur, TAUR_A, TAUR_B, "Q2_taur");
  tau_r = FMINBRsolution(Q2_taur, TAUR_A, TAUR_B);
}

std::function<double(const double)> PulseConverter::InjectPointCorrectionFunc(const double tau_0_tmp, const double tau_r_tmp) {
  const double Delta {tau_r_tmp * tau_r_tmp - 4 * tau_0_tmp * tau_0_tmp};
  if (Delta > 0) {
    const double tau_a {sqrt(Delta)};
    const double taup {.5 * (tau_r_tmp + tau_a)};
    const double taum {.5 * (tau_r_tmp - tau_a)};
    return [taup, taum](const double t) -> double {return (exp(-t / taup) - exp(-t / taum) ) / (taup - taum);};
  } else if (Delta < 0) {
    const double T {sqrt(-Delta)};
    const double A {2 * tau_r_tmp / ((tau_r_tmp * tau_r_tmp) - Delta)};
    const double B {2 * T / ((tau_r_tmp * tau_r_tmp) - Delta)};
    return [A, B, T](const double t) -> double {return 2 * exp(-A * t) * sin(B * t) / T;};
  } else {
    const double tau {.5 * tau_r_tmp};
    return [tau](const double t) -> double {return exp(-t / tau) * t / (tau * tau);};
  }
}

std::vector<double> PulseConverter::convolution(const std::vector<double> g1, const std::vector<double> g2, const int tail) {
  const int size {static_cast<int>(g1.size())};
  if (size <= tail || size != static_cast<int>(g2.size())) {
    std::cout << "ERROR: bad size of vector\n";
    return {0};
  }
  std::vector<double> v(size);
  for (int i{tail}; i < size; ++i) for (int j{}; j <= i; ++j) v[i - tail] += g1[i - j] * g2[j] * delta;
  return v;
}

std::vector<double> PulseConverter::w_out(const std::vector<double> ftran, const int tail) {
  return convolution(ftran, g_cali, tail);
}

double PulseConverter::Q2(const std::vector<double> ftran, const int tail) {
  std::vector<double> v {w_out(ftran, tail)};
  return std::inner_product(v.begin(), v.end(), v.begin(), 0.);
}

double PulseConverter::logQ2(const std::vector<double> ftran, const int tail) {
  std::vector<double> v {w_out(ftran, tail)};
  return log(std::inner_product(v.begin(), v.end(), v.begin(), 0.));
}

void PulseConverter::Plot1DFunctionGraph(std::function<double(const double)> func, const double a, const double b, const char* name) {
  std::unique_ptr<TGraph> g {std::make_unique<TGraph>()};
  for (int i = 0; i < 1000; ++i) g->SetPoint(i, a + i*(b-a)/1000, func(a + i*(b-a)/1000));
  TCanvas* c = new TCanvas();
  c->cd();
  g->GetXaxis()->SetLimits(a, b);
  g->Draw("apl");
  c->SaveAs(Form("%s.pdf", name));
  
  std::unique_ptr<TFile> fout {std::make_unique<TFile>(Form("%s.root", name), "recreate")};
  fout->Add(c);
  fout->Write();
  fout->Close();
  
}

void PulseConverter::PlotGWaves(const char* name) {
  std::unique_ptr<TGraph> gc {std::make_unique<TGraph>()};
  std::unique_ptr<TGraph> gp {std::make_unique<TGraph>()};
  for (int i = 0; i < size; ++i) {
    gc->SetPoint(i, i * delta, g_cali[i]);
    gp->SetPoint(i, i * delta, g_phys[i]);
  }
  gc->SetMarkerStyle(21); gp->SetMarkerStyle(21);
  gc->SetMarkerSize(.3); gp->SetMarkerSize(.3);
  gc->SetMarkerColor(kBlack); gp->SetMarkerColor(kRed);
  std::unique_ptr<TCanvas> c {std::make_unique<TCanvas>()};
  c->cd();
  gc->GetXaxis()->SetLimits(0, (size - 1) * delta); gp->GetXaxis()->SetLimits(0, (size - 1) * delta);
  gp->Draw("ap");
  gc->Draw("p");
  std::unique_ptr<TLine> line {std::make_unique<TLine>(0, 0, (size - 1) * delta, 0)};
  line->SetLineStyle(3);
  line->Draw();
  c->SaveAs(Form("%s.pdf", name));
  /*
  std::unique_ptr<TFile> fout {std::make_unique<TFile>(Form("%s.root", name), "recreate")};
  fout->Add(c.get());
  fout->Write();
  fout->Close();
  */
}

double PulseConverter::FMINBRsolution(std::function<double(const double)> func, double a, double b) {
  double x,v,w;
  double fx,fv,fw;
  const double r {(3.-sqrt(5.)) / 2}; // Gold section ratio
  const double epsilon {2.22045e-16};
  const double tol {epsilon};
  const double sqrt_epsilon {1.49012e-08};
  
  v = a + r * (b - a);
  fv = func(v);       /* First step - always gold section*/
  x = w = v;
  fx = fw = fv;

  while (true) {
    const double range {b - a};
    const double middle_range {(a + b) / 2};
    const double tol_act {sqrt_epsilon * fabs(x) + tol / 3};
    double new_step;

    if (fabs(x - middle_range) + range / 2 <= 2 * tol_act) return x;

    new_step = r * (x < middle_range ? b-x : a-x);

    // Decide if the interpolation can be tried
    if (fabs(x - w) >= tol_act) { // If x and w are distinct interpolatiom may be tried
      const double t {(x - w) * (fx - fv)};
      double q {(x - v) * (fx - fw)};
      double p {(x - v) * q - (x - w) * t};
      q = 2 * (q - t);
      if (q > 0) p = -p;
      else q = -q;
      // If x+p/q falls in [a,b] not too close to a and b, and isn't too large it is accepted
      // If p/q is too large then the gold section procedure can reduce [a,b] range to more extent
      if (fabs(p) < fabs(new_step * q) && p > q * (a - x + 2 * tol_act) && p < q * (b - x - 2 * tol_act)) new_step = p/q;
    }

    // Adjust the step to be not less than tolerance
    if (fabs(new_step) < tol_act) {
      if (new_step > 0) new_step = tol_act;
      else new_step = -tol_act;
    }
    
    { // Obtain the next approximation to min and reduce the enveloping range
      const double t {x + new_step}; // Tentative point for the min
      const double ft {func(t)};
      if (ft <= fx) {
	if (t < x) b = x;
	else a = x;
	v = w;  w = x;  x = t;
	fv=fw;  fw=fx;  fx=ft;
      } else {        		             
	if (t < x) a = t;                   
	else b = t;
	if (ft <= fw || w==x) {
	  v = w;  w = t;
	  fv=fw;  fw=ft;
	} else if (ft<=fv || v==x || v==w) {
	  v = t;
	  fv=ft;
	}
      }
    }

  }
}
