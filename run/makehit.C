#include <TROOT.h>
#include <TFile.h>
#include "vector"

void makehit()
{
  gInterpreter->GenerateDictionary("vector<vector<int> >","vector");

  TFile* file = new TFile("singleHit.root", "recreate");
  TTree* tree = new TTree("gend3pd", "gend3pd");

  Int_t           hitemb_n;
  vector<float>   hitemb_eta;
  vector<float>   hitemb_phi;
  vector<float>   hitemb_E;
  vector<float>   hitemb_Time;
  vector<unsigned int> hitemb_ID;
  Int_t           hitemec_n;
  vector<float>   hitemec_eta;
  vector<float>   hitemec_phi;
  vector<float>   hitemec_E;
  vector<float>   hitemec_Time;
  vector<unsigned int> hitemec_ID;
  Int_t           hitfcal_n;
  vector<float>   hitfcal_eta;
  vector<float>   hitfcal_phi;
  vector<float>   hitfcal_E;
  vector<float>   hitfcal_Time;
  vector<unsigned int> hitfcal_ID;
  Int_t           hithec_n;
  vector<float>   hithec_eta;
  vector<float>   hithec_phi;
  vector<float>   hithec_E;
  vector<float>   hithec_Time;
  vector<unsigned int> hithec_ID;
  Int_t           mc_n;
  vector<float>   mc_pt;
  vector<float>   mc_m;
  vector<float>   mc_eta;
  vector<float>   mc_phi;
  vector<int>     mc_status;
  vector<int>     mc_barcode;
  vector<int>     mc_pdgId;
  vector<float>   mc_charge;
  vector<vector<int> > mc_parents;
  vector<vector<int> > mc_children;
  vector<float>   mc_vx_x;
  vector<float>   mc_vx_y;
  vector<float>   mc_vx_z;
  vector<int>     mc_vx_barcode;
  vector<vector<int> > mc_child_index;
  vector<vector<int> > mc_parent_index;
  Int_t           AntiKt4TruthJets_n;
  vector<float>   AntiKt4TruthJets_E;
  vector<float>   AntiKt4TruthJets_pt;
  vector<float>   AntiKt4TruthJets_m;
  vector<float>   AntiKt4TruthJets_eta;
  vector<float>   AntiKt4TruthJets_phi;

  tree->Branch("hitemb_n", &hitemb_n);
  tree->Branch("hitemb_eta", &hitemb_eta);
  tree->Branch("hitemb_phi", &hitemb_phi);
  tree->Branch("hitemb_E", &hitemb_E);
  tree->Branch("hitemb_Time", &hitemb_Time);
  tree->Branch("hitemb_ID", &hitemb_ID);
  tree->Branch("hitemec_n", &hitemec_n);
  tree->Branch("hitemec_eta", &hitemec_eta);
  tree->Branch("hitemec_phi", &hitemec_phi);
  tree->Branch("hitemec_E", &hitemec_E);
  tree->Branch("hitemec_Time", &hitemec_Time);
  tree->Branch("hitemec_ID", &hitemec_ID);
  tree->Branch("hitfcal_n", &hitfcal_n);
  tree->Branch("hitfcal_eta", &hitfcal_eta);
  tree->Branch("hitfcal_phi", &hitfcal_phi);
  tree->Branch("hitfcal_E", &hitfcal_E);
  tree->Branch("hitfcal_Time", &hitfcal_Time);
  tree->Branch("hitfcal_ID", &hitfcal_ID);
  tree->Branch("hithec_n", &hithec_n);
  tree->Branch("hithec_eta", &hithec_eta);
  tree->Branch("hithec_phi", &hithec_phi);
  tree->Branch("hithec_E", &hithec_E);
  tree->Branch("hithec_Time", &hithec_Time);
  tree->Branch("hithec_ID", &hithec_ID);
  tree->Branch("mc_n", &mc_n);
  tree->Branch("mc_pt", &mc_pt);
  tree->Branch("mc_m", &mc_m);
  tree->Branch("mc_eta", &mc_eta);
  tree->Branch("mc_phi", &mc_phi);
  tree->Branch("mc_status", &mc_status);
  tree->Branch("mc_barcode", &mc_barcode);
  tree->Branch("mc_pdgId", &mc_pdgId);
  tree->Branch("mc_charge", &mc_charge);
  tree->Branch("mc_parents", &mc_parents);
  tree->Branch("mc_children", &mc_children);
  tree->Branch("mc_vx_x", &mc_vx_x);
  tree->Branch("mc_vx_y", &mc_vx_y);
  tree->Branch("mc_vx_z", &mc_vx_z);
  tree->Branch("mc_vx_barcode", &mc_vx_barcode);
  tree->Branch("mc_child_index", &mc_child_index);
  tree->Branch("mc_parent_index", &mc_parent_index);
  tree->Branch("AntiKt4TruthJets_n", &AntiKt4TruthJets_n);
  tree->Branch("AntiKt4TruthJets_E", &AntiKt4TruthJets_E);
  tree->Branch("AntiKt4TruthJets_pt", &AntiKt4TruthJets_pt);
  tree->Branch("AntiKt4TruthJets_m", &AntiKt4TruthJets_m);
  tree->Branch("AntiKt4TruthJets_eta", &AntiKt4TruthJets_eta);
  tree->Branch("AntiKt4TruthJets_phi", &AntiKt4TruthJets_phi);

  for (int i = 0; i < 50; ++i) {
    hitemb_n = 0;
    hitemec_n = 0;
    hitfcal_n = 0;
    hithec_n = 0;
    tree->Fill();
  }

  hitemb_n = 1;
  hitemb_eta = {0.0125000};
  hitemb_phi = {0.0460194};
  hitemb_E = {50.};
  hitemb_Time = {0.};
  hitemb_ID = {65};
  tree->Fill();

  for (int i = 0; i < 50; ++i) {
    hitemb_n = 0;
    hitemec_n = 0;
    hitfcal_n = 0;
    hithec_n = 0;
    tree->Fill();
  }

  tree->Write();
}
