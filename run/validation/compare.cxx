#include "struct.h"
#include "reconstructed.C"

void compare() {
  TFile* fref = new TFile("../storage/ReconstructedSequences/reconstructedRef.root");
  TTree* tref = (TTree*)fref->Get("reconstructed");
  reconstructed rref(tref);
  variables vref = rref.ExportVariables(0);

  TFile* fval = new TFile("../storage/ReconstructedSequences/validation.root");
  TTree* tval = (TTree*)fval->Get("reconstructed");
  reconstructed rval(tval);
  variables vval = rval.ExportVariables(0);
  
  if (vref != vval) std::cout << "Reconstructed sequences are different from the reference!\n";
  else std::cout << "Reconstructed sequences are consistent with the reference\n";
}
