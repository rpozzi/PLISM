import sys,os
sys.path[0] = os.path.abspath(os.environ['LAR_SIMLATOR_MAIN_DIR'] + '/run/tool')
sys.argv.append('-b')
import Functions as fn
import HitSummarizerModule
import DigitizerModule
import ReconstructorModule

CellPropertyFileName = os.path.join(fn.CELLPROPDIR, 'CellProperty_968435712.root')

SignalHitSummaryFile = 'HitSummaryTest.root'
HitSummarizer = HitSummarizerModule.HitSummarizer()
HitSummarizer.OutputFileName = SignalHitSummaryFile
HitSummarizer.CellPropertyFileName = CellPropertyFileName
HitSummarizer.NEvent = 10000000
# Uniform distribution
HitSummarizer.EtMin = 0 #MeV
HitSummarizer.EtMax = 500000 #MeV
HitSummarizer.TauMin = 0 #ns
HitSummarizer.TauMax = 0 #ns
HitSummarizer.makeSignal()

DigitSequenceFileName = 'digitSequence_Et0-500_Tau0.root'
Digitizer = DigitizerModule.Digitizer(CellPropertyFileName)
Digitizer.NBC = 5000000
Digitizer.mu = 80
Digitizer.phase = 0
Digitizer.TrainPattern = [1, 0]
Digitizer.SignalPattern = [1, 49]
Digitizer.OutputFileName = DigitSequenceFileName
Digitizer.SignalHitSummaryFile = SignalHitSummaryFile
Digitizer.LowPtPileupHitSummaryFile = 'HitSummary_LowPt_968435712.root'
Digitizer.HighPtPileupHitSummaryFile = 'HitSummary_HighPt_968435712.root'
Digitizer.OutputBranch_PileupTrueEt = False
Digitizer.OutputBranch_PileupTrueTau = False
Digitizer.OutputBranch_PileupTrueEtSum = False
Digitizer.sequence()

Reconstructor = ReconstructorModule.Reconstructor(DigitSequenceFileName)
Reconstructor.OutputFileName = 'reconstructed_Sequence_Et0-500_Tau0.root'
Reconstructor.OFCFileName = os.path.join(fn.OFCDIR, 'OFC_968435712_athena_mu80.root')
Reconstructor.OutputBranch_PileupTrueEt = False
Reconstructor.OutputBranch_PileupTrueTau = False
Reconstructor.OutputBranch_PileupTrueEtSum = False
Reconstructor.reconstruct()
