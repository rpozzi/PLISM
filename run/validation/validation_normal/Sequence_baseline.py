import sys,os
sys.path[0] = os.path.abspath(os.environ['LAR_SIMLATOR_MAIN_DIR'] + '/run/tool')
sys.argv.append('-b')
import Functions as fn
import HitSummarizerModule
import DigitizerModule
import ReconstructorModule

CellPropertyFileName = os.path.join(fn.CELLPROPDIR, 'CellProperty_968435712.root')

DigitSequenceFileName = 'digitSequence_baseline.root'
Digitizer = DigitizerModule.Digitizer(CellPropertyFileName)
Digitizer.NBC = 50000000
Digitizer.mu = 80
Digitizer.phase = 0
Digitizer.TrainPattern = [48, 8, 48, 8, 48, 32]
Digitizer.OutputFileName = DigitSequenceFileName
Digitizer.LowPtPileupHitSummaryFile = 'HitSummary_LowPt_968435712.root'
Digitizer.HighPtPileupHitSummaryFile = 'HitSummary_HighPt_968435712.root'
Digitizer.OutputBranch_PileupTrueEt = False
Digitizer.OutputBranch_PileupTrueTau = False
Digitizer.OutputBranch_PileupTrueEtSum = False
Digitizer.sequence()