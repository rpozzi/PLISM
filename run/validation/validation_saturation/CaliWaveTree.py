import sys,os
sys.path[0] = os.path.abspath(os.environ['LAR_SIMLATOR_MAIN_DIR'] + '/run/tool')
import Functions as fn
import CellPropertyGeneratorModule

LArIDtranslatorList = "LArIDtranslator_test.txt"
target = ["ONL_ID", "SC_ONL_ID", "CL"]
condition = "FTNAME like 'I03L'"
overlap_canceller = True
fn.MakeListFromLArIDtranslator(target, condition, LArIDtranslatorList, overlap_canceller)


CaliWaveTree = CellPropertyGeneratorModule.CaliWaveTree(
    'CaliWave_test.root'
)
CaliWaveTree.ID_SCID_CL_fileName = 'LArIDtranslator_test.txt'
CaliWaveTree.DAC2MeVFileName = 'LArDAC2MeV.root'
CaliWaveTree.LSB_delayPeak()

CaliWaveTree.PedFileName = 'LArPedAutoCorr_00390803.root'
CaliWaveTree.PulseTree()

LSBTree = CellPropertyGeneratorModule.LSBTree(
    'lsb_CaliWave_test.root'
)
#1 Detector ('EMB' or 'EMEC')
#2 LSB reference file
#3 Upper limit of LSB in output graph (max value is set as upper limit if you set 0)
LSBTree.LSBvsEta('EMB', os.path.join(fn.CELLPROPDIR, 'LSBreference.txt'), 600)

PulseShapeTree = CellPropertyGeneratorModule.PulseShapeTree(
    'pulse_CaliWave_test.root'
)
PulseShapeTree.PedAlreadySubtracted = True
PulseShapeTree.LSBFileName = 'lsb_CaliWave_test.root'
PulseShapeTree.GenerateCellProperty()
PulseShapeTree.DrawPulseShape()
