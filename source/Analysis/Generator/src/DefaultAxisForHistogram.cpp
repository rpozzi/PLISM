#include "../inc/Generator.hpp"

namespace PLISMAnalysis
{
    double Generator::DefaultAxisLowForHistogram(int InputFileLabel,
                                                 const std::string& DataName,
                                                 bool MeVtoGeV)
    {
        if (std::regex_search(DataName, std::regex("EtResolution"))) {
            double emax{AnalysisData.MinMaxEts_MeV[InputFileLabel].second};
            return -0.03 * 500000. / emax;
        } else if (std::regex_search(DataName, std::regex("TauResolution"))) {
            double emax{AnalysisData.MinMaxEts_MeV[InputFileLabel].second};
            return -3.0 * 50000. / emax;
        } else if (std::regex_search(DataName, std::regex("EtTau"))) {
            double et{
                (AnalysisData.MinMaxEts_MeV[InputFileLabel].first) *
                    (AnalysisData.MinMaxTaus_ns[InputFileLabel].first - 2) -
                AnalysisData.MinMaxEts_MeV[InputFileLabel].second};
            return std::round(sgn<double>(et) * abs(et) * 1.0 /
                              (1. + 999. * MeVtoGeV));
        } else if (std::regex_search(DataName, std::regex("Et"))) {
            double e{AnalysisData.MinMaxEts_MeV[InputFileLabel].first};
            return std::round(sgn<double>(e) * abs(e) * 1.0 /
                              (1. + 999. * MeVtoGeV));
        } else if (std::regex_search(DataName, std::regex("RecTau"))) {
            double tmin{AnalysisData.MinMaxTaus_ns[InputFileLabel].first};
            return std::round(tmin - 5);
        } else if (std::regex_search(DataName, std::regex("Tau"))) {
            double tmin{AnalysisData.MinMaxTaus_ns[InputFileLabel].first};
            return std::round(tmin);
        } else {
            std::cerr << WarningrMessage << "::DefaultAxisLowForHistogram: "
                      << "Unenable to determine AxisLow.\n";
            exit(EXIT_FAILURE);
        }
    }

    double Generator::DefaultAxisUpForHistogram(int InputFileLabel,
                                                const std::string& DataName,
                                                bool MeVtoGeV)
    {
        if (std::regex_search(DataName, std::regex("EtResolution"))) {
            double etmax{AnalysisData.MinMaxEts_MeV[InputFileLabel].second};
            return 0.03 * 500000. / etmax;
        } else if (std::regex_search(DataName, std::regex("TauResolution"))) {
            double emax{AnalysisData.MinMaxEts_MeV[InputFileLabel].second};
            return 3.0 * 50000. / emax;
        } else if (std::regex_search(DataName, std::regex("EtTau"))) {
            double et{(AnalysisData.MinMaxEts_MeV[InputFileLabel].second) *
                      (AnalysisData.MinMaxTaus_ns[InputFileLabel].second + 2)};
            return std::round(sgn<double>(et) * abs(et) * 1.0 /
                              (1. + 999. * MeVtoGeV));
        } else if (std::regex_search(DataName, std::regex("Et"))) {
            double e{AnalysisData.MinMaxEts_MeV[InputFileLabel].second};
            return std::round(sgn<double>(e) * abs(e) * 1.0 /
                              (1. + 999. * MeVtoGeV));
        } else if (std::regex_search(DataName, std::regex("RecTau"))) {
            double tmax{AnalysisData.MinMaxTaus_ns[InputFileLabel].second};
            return std::round(tmax + 5);
        } else if (std::regex_search(DataName, std::regex("Tau"))) {
            double tmax{AnalysisData.MinMaxTaus_ns[InputFileLabel].second};
            return std::round(tmax);
        } else {
            std::cerr << WarningrMessage << "::DefaultAxisUpForHisogram: "
                      << "Unenable to determine AxisLUP.\n";
            exit(EXIT_FAILURE);
        }
    }

}  // namespace PLISMAnalysis
