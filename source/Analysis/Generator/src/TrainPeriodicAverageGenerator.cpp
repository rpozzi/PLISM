#include <TTreeReader.h>

#include "../inc/Generator.hpp"

namespace PLISMAnalysis
{
    TGraph* Generator::TrainPeriodicAverageGenerator(
        const int InputFileLabel, const std::string& DataName, bool isMeVtoGeV)
    {
        AnalysisData.InputFileLabelHandler(InputFileLabel);
        auto&& fin = AnalysisData.InputFiles[InputFileLabel];
        if (DataName == std::string{"BgAnalogEt"}) {
            TTreeReader reader{
                AnalysisData.InputFileTypes[InputFileLabel].c_str(), fin};
            TTreeReaderValue<std::deque<double>> Data{reader, DataName.c_str()};
            reader.Next();
            std::string title{";time [ns];" +
                              DefaultAxisLabel(DataName, isMeVtoGeV)};
            return TrainPeriodicAverageGenerator(
                InputFileLabel, *Data, isMeVtoGeV ? 0.001 : 1., title);
        } else if (DataName == std::string{"RecEt"}) {
            TTreeReader reader{
                AnalysisData.InputFileTypes[InputFileLabel].c_str(), fin};
            TTreeReaderValue<std::vector<double>> Data{reader,
                                                       DataName.c_str()};
            reader.Next();
            std::string title{";time [ns];" +
                              DefaultAxisLabel(DataName, isMeVtoGeV)};
            return TrainPeriodicAverageGenerator(
                InputFileLabel, *Data, isMeVtoGeV ? 0.001 : 1., title);
        } else if (DataName == std::string{"Digit"}) {
            TTreeReader reader{
                AnalysisData.InputFileTypes[InputFileLabel].c_str(), fin};
            TTreeReaderValue<std::deque<int>> Data{reader, DataName.c_str()};
            reader.Next();
            std::string title{";time [ns];" +
                              DefaultAxisLabel(DataName,isMeVtoGeV)};
            return TrainPeriodicAverageGenerator(
                InputFileLabel, *Data, isMeVtoGeV ? 0.001 : 1., title);
        } else if (DataName == std::string{"DigitLSB"}) {
            TTreeReader reader{
                AnalysisData.InputFileTypes[InputFileLabel].c_str(), fin};
            TTreeReaderValue<std::deque<int>> Data{reader, "Digit"};
            TTreeReaderValue<float> LSB(reader, "LSB");
            reader.Next();
            std::string title{";time [ns];" +
                              DefaultAxisLabel(DataName, isMeVtoGeV)};
            return TrainPeriodicAverageGenerator(
                InputFileLabel, *Data, isMeVtoGeV ? 0.001 * *LSB : 1. * *LSB,
                title);
        } else if (DataName == std::string{"PileupTrueEtSum"}) {
            TTreeReader reader{
                AnalysisData.InputFileTypes[InputFileLabel].c_str(), fin};
            TTreeReaderValue<std::vector<double>> Data{reader,
                                                       "PileupTrueEtSum"};
            reader.Next();
            std::string title{";time [ns];" +
                              DefaultAxisLabel(DataName, isMeVtoGeV)};
            return TrainPeriodicAverageGenerator(
                InputFileLabel, *Data, isMeVtoGeV ? 0.001 : 1., title);
        } else {
            std::cerr << ErrorMessage << "::TrainPeriodicAverageGenerator: "
                      << "UnknownDataName.\n";
            exit(EXIT_FAILURE);
        }
    }
}  // namespace PLISMAnalysis