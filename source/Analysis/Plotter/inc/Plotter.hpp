#pragma once

#include "../../Generator/inc/Generator.hpp"

namespace PLISMAnalysis
{
    class Plotter
    {
    private:
        DataBase database;
        Generator generator;

        static inline std::ostream& ErrorMessage(std::ostream& os)
        {
            os << "Error in PLISMAnalysis::Plotter";
            return os;
        }
        static inline std::ostream& WarningrMessage(std::ostream& os)
        {
            os << "Warning in PLISMAnalysis::Plotter";
            return os;
        }

        // Default Legend Generator
        std::string DefaultLegendGenerator(const int InputFileLabel,
                                           const std::string& legend);

        // Helper for Plot of TH2D or TGraph
        void TH2DPlotterHelper(TH2D*& h, const std::string& OuptutFilename);
        void TGraphPlotterHelper(TGraph*& g, const std::string& Option,
                                 const double MarkerSize,
                                 const std::string& OutputFilename);

    public:
        // constructor
        Plotter(const std::vector<std::string>& InputFilenames)
            : database(DataBase{InputFilenames}),
              generator(Generator{database}){};

        // Plot of TH2D (xDataName vs yDataName) of a
        // InputFile. DataName should be "RecEt",
        // "RecTau" , "RecEtTau", "SignalTrueEt",
        // "SignalTrueTau", "SignalTrueEtTau",
        // EtResolution" or "TauResoolution".
        // DefaultAxisLabel and DefaultDataScaler are
        // applied.
        void TH2DPlotter(const int InputFileLabel, const std::string& xDataName,
                         const std::string& yDataName, const int nbinsx,
                         const double xlow, const double xup, const int nbinsy,
                         const double ylow, const double yup,
                         bool isOnlyEventBCDataUsed = true,
                         const std::string& OutputFileName = "");
        void TH2DPlotter(const std::string& InputFilename,
                         const std::string& xDataName,
                         const std::string& yDataName, const int nbinsx,
                         const double xlow, const double xup, const int nbinsy,
                         const double ylow, const double yup,
                         bool isOnlyEventBCDataUsed = true,
                         const std::string& OutputFileName = "");

        void TH2DPlotter(
            const std::string& xDataName, const std::string& yDataName,
            const int nbinsx, const double xlow, const double xup,
            const int nbinsy, const double ylow, const double yup,
            bool isOnlyEventBCDataUsed = true,
            const std::string& OutputFileName = "./figure/TH2DPlot.pdf");

        // Plot of TH2D (xDataName vs yDataName) of  a
        // InputFile. DataName should be "RecEt",
        // "RecTau" , "RecEtTau", "SignalTrueEt",
        // "SignalTrueTau", "SignalTrueEtTau",
        // EtResolution" or "TauResoolution".
        // DefaultAxisLabel , DefaultDataScaler,
        // Default nbins
        // (=200), DefaultAxisLow/Up are applied.
        void TH2DPlotter(const int InputFileLabel, const std::string& xDataName,
                         const std::string& yDataName,
                         bool isOnlyEventBCDataUsed = true,
                         const std::string& OutputFileName = "");
        void TH2DPlotter(const std::string& InputFilename,
                         const std::string& xDataName,
                         const std::string& yDataName,
                         bool isOnlyEventBCDataUsed = true,
                         const std::string& OutputFileName = "");
        void TH2DPlotter(const std::string& xDataName,
                         const std::string& yDataName,
                         bool isOnlyEventBCDataUsed = true,
                         const std::string& OutputFileName = "");

        // combine All TH2Ds of a InputFile to a single pdf file.
        void TH2DPlotterALL(const int InputFileLabel,
                            bool isOnlyEventBCDataUsed = true,
                            const std::string& OutputFileName = "");

        // Plot of scatter diagram(xDataName vs
        // yDataName) of a InputFile. DataName should
        // be "RecEt", "RecTau" , "RecEtTau",
        // "SignalTrueEt", "SignalTrueTau",
        // "SignalTrueEtTau", EtResolution" or
        // "TauResoolution". DefaultAxisLabel and
        // DefaultDataScaler are applied.
        void ScatterPlotter(const int InputFileLabel,
                            const std::string& xDataName,
                            const std::string& yDataName, const double xlow,
                            const double xup, const double ylow,
                            const double yup,
                            bool isOnlyEventBCDataUsed = false,
                            const std::string& OutputFilename = "");
        void ScatterPlotter(const std::string& InputFilename,
                            const std::string& xDataName,
                            const std::string& yDataName, const double xlow,
                            const double xup, const double ylow,
                            const double yup,
                            bool isOnlyEventBCDataUsed = false,
                            const std::string& OutputFilename = "");
        void ScatterPlotter(
            const std::string& xDataName, const std::string& yDataName,
            const double xlow, const double xup, const double ylow,
            const double yup, bool isOnlyEventBCDataUsed = false,
            const std::string& OutputFilename = "./figure/ScatterPlot.pdf");

        // Plot of scatter diagram(xDataName vs
        // yDataName) of a InputFile. DataName should
        // be "RecEt", "RecTau" , "RecEtTau",
        // "SignalTrueEt", "SignalTrueTau",
        // "SignalTrueEtTau", EtResolution" or
        // "TauResoolution". DefaultAxisLabel ,
        // DefaultDataScaler, DefaultAxisLow/Up are applied.
        void ScatterPlotter(const int InputFileLabel,
                            const std::string& xDataName,
                            const std::string& yDataName,
                            bool isOnlyEventBCDataUsed = false,
                            const std::string& OutputFilename = "");
        void ScatterPlotter(const std::string& InputFilename,
                            const std::string& xDataName,
                            const std::string& yDataName,
                            bool isOnlyEventBCDataUsed = false,
                            const std::string& OutputFilename = "");
        void ScatterPlotter(
            const std::string& xDataName, const std::string& yDataName,
            bool isOnlyEventBCDataUsed = false,
            const std::string& OutputFilename = "./figure/ScatterPlot.pdf");

        // OverlayPlot of scatter diagram(xDataName vs
        // yDataName) of all InputFiles. DataName
        // should be "RecEt", "RecTau" , "RecEtTau",
        // "SignalTrueEt", "SignalTrueTau",
        // "SignalTrueEtTau", EtResolution" or
        // "TauResoolution". legend should be a combination consisting of “mu” ,
        // “SignalTrueEt”, “SignalTrueTau”, “TrainPattern”, “SignalPattern” like
        // "muSignalTureEt".
        void ScatterOverlayPlotter(const ::std::string& xDataName,
                                   const std::string& yDataName,
                                   const std::string& legend,
                                   bool isOnlyEventBCDataUsed = false,
                                   const std::string& OutputFilename = "");

        // combine All scatter diagrams of a InputFile to a single pdf file
        void ScatterPlotterALL(const int InputFileLabel,
                               bool isOnlyEventBCDataUsed,
                               const std::string& OutputFoldername);
        inline void ScatterPlotterALL(const std::string& InputFileName,
                                      bool isOnlyEventBCDataUsed,
                                      const std::string& OutputFoldername)
        {
            ScatterPlotterALL(database.InputFilenameToLabel[InputFileName],
                              isOnlyEventBCDataUsed, OutputFoldername);
        }
        inline void ScatterPlotterALL(bool isOnlyEventBCDataUsed,
                                      const std::string& OutputFoldername)
        {
            ScatterPlotterALL(0, isOnlyEventBCDataUsed, OutputFoldername);
        }

        // Plot of Sequence("Option" vs time) of a
        // InputFile. Option : a(AnalogEt),
        // b(BgAnalogEt), d(Digit*LSB), r(RecEt),
        // t(TrueEt), s(Selected Et or passed tau
        // cirteria)
        void SequencePlotter(const int InputFileLabel, int MinTime_ns = 1000,
                             int MaxTime_ns = 2000,
                             const std::string& Option = "drts",
                             const std::string& OutputFileName = "",
                             bool isMeVtoGeV = true);

        inline void SequencePlotter(const std::string& InputFilename,
                                    int MinTime_ns = 1000,
                                    int MaxTime_ns = 2000,
                                    const std::string& Option = "drts",
                                    const std::string& OutputFileName = "",
                                    bool isMeVtoGeV = true)

        {
            database.InputFilenameHandler(InputFilename);
            SequencePlotter(database.InputFilenameToLabel[InputFilename],
                            MinTime_ns, MaxTime_ns, Option, OutputFileName,
                            isMeVtoGeV);
        };

        void SequencePlotter(int MinTime_ns = 1000, int MaxTime_ns = 2000,
                             const std::string& Option = "drts",
                             const std::string& OutputFileName = "./figure/SequencePlot.pdf",
                             bool isMeVtoGeV = true);

        // OverlayPlot of Sequence(DataName vs time) of
        // all InputFiles. DataName should be
        // “AnalogEt”, “BgAnalogEt”, “DigitLSB”,
        // “RecEt”, or “TrueEt”. legend should be a combination consisting of
        // “mu” , “SignalTrueEt”, “SignalTrueTau”, “TrainPattern”,
        // “SignalPattern” like "muSignalTureEt".
        void SequenceOverlayPlotter(const std::string& DataName,
                                    const std::string& legend,
                                    int MinTime_ns = 0, int MaxTime_ns = 10000,
                                    const std::string& OutputFileName = "",
                                    bool isMeVtoGeV = true);

        // Plot of TrainStructure-Period average of DataName of a InputFile.
        // DataName should be "BgAnalogEt", "RecEt", "Digit" or ”DigitLSB”.
        void TrainPeriodicAveragePlotter(const int InputFileLabel,
                                         const std::string& DataName,
                                         const std::string& OutputFileName = "",
                                         bool isMeVtoGeV = false);
        inline void TrainPeriodicAveragePlotter(
            const std::string& InputFilename, const std::string& DataName,
            const std::string& OutputFileName = "", bool isMeVtoGeV = false)
        {
            database.InputFilenameHandler(InputFilename);
            TrainPeriodicAveragePlotter(
                database.InputFilenameToLabel[InputFilename], DataName,
                OutputFileName, isMeVtoGeV);
        };

        // OverlayPlot of TrainStructure-Period average of DataName of all
        // InputFiles. DataName should be "BgAnalogEt", "RecEt", "Digit"
        // or ”DigitLSB”. legend should be a combination consisting of
        // “mu” , “SignalTrueEt”, “SignalTrueTau”, “TrainPattern”,
        // “SignalPattern” like "muSignalTureEt".
        void TrainPeriodicAverageOverlayPlotter(
            const std::string& DataName, const std::string& legend,
            const std::string& OutputFileName = "", bool isMeVtoGeV = false);

        // DataName should be "BgAnalogEt", "RecEt", "Digit", and "DigitLSB".
        void TrainPeriodicRMSPlotter(const int InputFileLabel,
                                     const std::string& DataName,
                                     const std::string& OutputFileName = "",
                                     bool isMeVtoGeV = false);
        // legend should be a combination consisting of
        // “mu” , “SignalTrueEt”, “SignalTrueTau”, “TrainPattern”,
        // “SignalPattern” like "muSignalTureEt".
        void TrainPeriodicRMSOverlayPlotter(
            const std::string& DataName, const std::string& legend,
            const std::string& OutputFileName = "", bool isMeVtoGeV = false);

        // Plot of TrainStructure-Period RMS of DataName of a InputFile.
        // DataName should be "BgAnalogEt", "RecEt", "Digit" or ”DigitLSB”.
        inline void TrainPeriodicRMSPlotter(
            const std::string& InputFilename, const std::string& DataName,
            const std::string& OutputFileName = "", bool isMeVtoGeV = false)
        {
            database.InputFilenameHandler(InputFilename);
            TrainPeriodicRMSPlotter(
                database.InputFilenameToLabel[InputFilename], DataName,
                OutputFileName, isMeVtoGeV);
        };
    };
}  // namespace PLISMAnalysis
