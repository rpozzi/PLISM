#include <regex>

#include "../inc/Plotter.hpp"

namespace PLISMAnalysis
{
    std::string Plotter::DefaultLegendGenerator(const int InputFileLabel,
                                                const std::string& legend)
    {
        std::string leg{""};
        if (std::regex_search(legend, std::regex{"mu"})) {
            leg += std::string{"  #mu = " +
                               std::to_string(static_cast<int>(
                                   std::round(database.Mus[InputFileLabel])))};
        }
        if (std::regex_search(legend, std::regex{"SignalTrueEt"})) {
            leg += std::string{
                "  TrueE_{T} = " +
                std::to_string(static_cast<int>(std::round(
                    database.MinMaxEts_MeV[InputFileLabel].first / 1000.))) +
                "-" +
                std::to_string(static_cast<int>(std::round(
                    database.MinMaxEts_MeV[InputFileLabel].second / 1000.))) +
                " [GeV]"};
        }
        if (std::regex_search(legend, std::regex{"SignalTrueTau"})) {
            leg += std::string{
                "  True#tau = " +
                std::to_string(static_cast<int>(
                    std::round(database.MinMaxTaus_ns[InputFileLabel].first))) +
                "-" +
                std::to_string(static_cast<int>(std::round(
                    database.MinMaxTaus_ns[InputFileLabel].second))) +
                " [ns]"};
        }
        if (std::regex_search(legend, std::regex{"TrainPattern"})) {
            std::string tmpleg{""};
            const auto& TrainPattern = database.TrainPatterns[InputFileLabel];
            int ctr{0};
            for (auto it = TrainPattern.begin(); it != TrainPattern.end();
                 it++, ctr++) {
                tmpleg += std::to_string(*it);
                if (ctr % 2 == 0) {
                    tmpleg += "b";
                } else {
                    tmpleg += "e";
                }
            }
            leg += "  TrainPattern = " + tmpleg;
        }
        if (std::regex_search(legend, std::regex{"SignalPattern"})) {
            std::string tmpleg{""};
            const auto& SignalPattern = database.SignalPatterns[InputFileLabel];
            int ctr{0};
            for (auto it = SignalPattern.begin(); it != SignalPattern.end();
                 it++, ctr++) {
                tmpleg += std::to_string(*it);
                if (ctr % 2 == 0) {
                    tmpleg += "e";
                } else {
                    tmpleg += "b";
                }
            }
            leg += "  SignalPattern = " + tmpleg;
        }
        return leg;
    }
}  // namespace PLISMAnalysis