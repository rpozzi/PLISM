#pragma once
#include <boost/filesystem.hpp>
#include <cstdlib>
#include <iostream>
#include <string>
#include "Message.h"

class FileGenerator
{
  public:
    FileGenerator(const boost::filesystem::path& outputPath) :
        m_path(boost::filesystem::absolute(outputPath))
    {
        Message::Info("Opening output file " + m_path.string());
    }

    virtual ~FileGenerator()
    {
        if (not m_path.empty()) Message::Info("Closed output file " + m_path.string());
    }

  private:
    const boost::filesystem::path m_path;
};
