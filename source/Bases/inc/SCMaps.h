#ifndef SCMaps_h
#define SCMaps_h
#include <TChain.h>
#include <TFile.h>
#include <TROOT.h>
#include <iostream>
#include <map>
#include <vector>
#include "Message.h"
#include "Structures.h"

class SCMaps
{
  public:
    static void fill(ChannelIdMap& theMap);
    static void fill(ChannelIdVector& theVector);
    static void fill(ChannelIdAngleMap& theMap);
    void fill(GOFCDataMap& theMap);

  private:
    enum AtlasID { LArEM,
                   LArHEC,
                   LArFCal,
                   LArTile };
    enum EMSubsystem { EMB,
                       InnerEMEC,
                       OuterEMEC };
    enum EMLayer { presampler,
                   front,
                   middle,
                   back };
    enum TileSubsystem { barrel,
                         extbarrel,
                         gap,
                         gapscin };
    enum TileSample { A  = 0,
                      B  = 1,
                      BC = 1,
                      C  = 1,
                      D  = 2,
                      E  = 3,
                      X  = 4 };
    static unsigned int getID(AtlasID AtlasID, EMSubsystem subsystem, EMLayer layer);
    static unsigned int getID(AtlasID AtlasID, int region);
    static unsigned int getID(AtlasID AtlasID, TileSubsystem subsystem, TileSample sample);
};

#endif // SCMaps_h
