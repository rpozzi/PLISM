/// @addtogroup CellPropertyGenerator
/// @{

#pragma once
#include "AREUSNoiseTree.h"
#include "SCMaps.h"
#include "Structures.h"
#include "Tree.h"

namespace CellPropertyGenerator
{
    /// Class to extract information from AREUS pulse shape file
    class AREUSPulseShapeTree : public Tree
    {
      public:
        /// @param inputFileName Input AREUS pulse shape file name
        AREUSPulseShapeTree(const std::string &inputFileName) :
            Tree(inputFileName, "PulseShapes")
        {
            samples = 0;
            fChain->SetBranchAddress("eta", &eta, &b_eta);
            fChain->SetBranchAddress("layer", &layer, &b_layer);
            fChain->SetBranchAddress("subsystem", &subsystem, &b_subsystem);
            fChain->SetBranchAddress("eT", &eT, &b_eT);
            fChain->SetBranchAddress("samples", &samples, &b_samples);
            fChain->SetBranchAddress("sampperbc", &sampperbc, &b_sampperbc);
        }

        void Loop(); ///< Set internal property

        ///
        /// @brief Output pulse shape file
        /// @param noiseFileName Input AREUS noise file name
        /// @param outputFileName Output pulse shape file name
        ///
        void generate(const std::string &noiseFileName, const std::string &outputFileName);

      private:
        Float_t eta;
        Int_t layer;
        Int_t subsystem;
        Float_t eT;
        std::vector<float> *samples;
        Int_t sampperbc;

        TBranch *b_eta;
        TBranch *b_layer;
        TBranch *b_subsystem;
        TBranch *b_eT;
        TBranch *b_samples;
        TBranch *b_sampperbc;

        struct Property {
            Float_t eT;
            Int_t sampperbc;
            std::deque<double> gFunction;
            bool operator<(const Property &obj) const { return eT < obj.eT; }
        };
        std::unordered_map<unsigned int, std::map<Float_t, std::vector<Property> > >
          m_propertyMap;
    };
} // namespace CellPropertyGenerator
/// @}
