/// @addtogroup CellPropertyGenerator
/// @{

#pragma once
#include "Tools.h"
#include "TreeReader.h"

namespace CellPropertyGenerator
{
    /// Class to extract information from P1CalibrationProcessing AUTOCORR tree
    class AutoCorrTree : public TreeReader
    {
      public:
        /// @param InputFileName Input AUTOCORR file name
        AutoCorrTree(const std::string& InputFileName) :
            TreeReader(InputFileName, "AUTOCORR"),
            channelId(m_reader, "channelId"),
            covr(m_reader, "covr") {}

        ///
        /// @brief Return channelid vs autocorrelation map
        /// Autocorrelation is written in vector
        ///
        std::unordered_map<Int_t, std::vector<Float_t>> getAutoCorr();

      private:
        TTreeReaderValue<Int_t> channelId;
        TTreeReaderArray<Float_t> covr;
    };
} // namespace CellPropertyGenerator
/// @}
