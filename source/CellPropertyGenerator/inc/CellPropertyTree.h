/// @addtogroup CellPropertyGenerator
/// @{

#pragma once
#include <deque>
#include "CellPropertyTreeBase.h"
#include "Tools.h"

namespace CellPropertyGenerator
{
    /// Class to extract information from cell property tree
    class CellPropertyTree : public CellPropertyTreeBase
    {
      public:
        /**
         * @brief Construct a new CellPropertyTree object
         * 
         * @param inputFileName Input cell property file name
         */
        CellPropertyTree(const std::string& inputFileName) :
            CellPropertyTreeBase(inputFileName) {}

        ///
        /// @brief Plot pedestal in ADC as function of eta
        /// @param upper_limit Upeer limit of y axis
        ///
        void PlotPedestalADC(const float upper_limit);

        ///
        /// @brief Plot pedestal in GeV as function of eta
        /// @param upper_limit Upeer limit of y axis
        ///
        void PlotPedestalGeV(const float upper_limit);

        ///
        /// @brief Plot pedestal RMS in ADC as function of eta
        /// @param upper_limit Upeer limit of y axis
        ///
        void PlotRMSADC(const float upper_limit);

        ///
        /// @brief Plot pedestal RMS in MeV as function of eta
        /// @param upper_limit Upeer limit of y axis
        ///
        void PlotRMSMeV(const float upper_limit);
    };
} // namespace CellPropertyGenerator
/// @}
