/// @addtogroup CellPropertyGenerator
/// @{

#pragma once
#include "AREUSPulseShapeTree.h"

namespace CellPropertyGenerator
{
    /// Class to generate cell property tree
    class DirectGenerator
    {
      public:
        /**
         * @brief Construct a new DirectGenerator object
         * 
         */
        DirectGenerator(){};

        ///
        /// @brief Generate cell property file from AREUS noise and pulse shape data
        /// @param PulseShapeFileName AREUS pulse shape file name
        /// @param NoiseFileName AREUS noise file name
        /// @param OutputFileName Output cell property file name
        ///
        void GenerateFromAREUSPulseShapeTree(const std::string& PulseShapeFileName, const std::string& NoiseFileName, const std::string& OutputFileName);

        void GenerateFromCalibFrameWork(); ///< Generate cell property file from P1CalibrationProcessing digit tree

        std::string PatternFileName;            ///< Input pulse pattern file name
        std::string ID_SCID_CL_FileName;        ///< Input (LArcellID, SCID, CL) list txt file name
        std::string DigitFileName;              ///< Input P1CalibrationProcessing digit file name
        std::string PedestalFileName;           ///< Input P1CalibrationProcessing pedestal file name
        std::string DAC2MeVFileName;            ///< Input DAC2MeV file name
        int NTriggers{};                        ///< Number of triggers used in the calibration run
        int NSamples{};                         ///< Number of samples per pulse
        std::string Detector;                   ///< Subdetector ('EMB' or 'EMEC'. Combination of multiple subdetectors is not considered.)
        std::string LSBReferenceFileName;       ///< LSB reference file which is already prepared in this repository
        float LSBPlotUpperLimit{};              ///< Upper limit of LSB in output graph (max value is set as the upper limit if you set 0)
        std::string OutputCellPropertyFileName; ///< Cell property file name to be output
        int BCShift{};                          ///< Option to cut the beginning of pulse in BC
    };
} // namespace CellPropertyGenerator
/// @}
