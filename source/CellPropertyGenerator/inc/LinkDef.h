#ifdef __CINT__

#    pragma link off all globals;
#    pragma link off all classes;
#    pragma link off all functions;

#    pragma link C++ class Tree;
#    pragma link C++ class TreeReader;
#    pragma link C++ class CellPropertyTreeBase;
#    pragma link C++ class LArIDtranslator;
#    pragma link C++ class SCMaps;
#    pragma link C++ class CellPropertyGenerator::AREUSNoiseTree;
#    pragma link C++ class CellPropertyGenerator::AREUSPulseShapeTree;
#    pragma link C++ class CellPropertyGenerator::PulsePattern;
#    pragma link C++ class CellPropertyGenerator::PatternCounter;
#    pragma link C++ class CellPropertyGenerator::PedestalTree;
#    pragma link C++ class CellPropertyGenerator::AutoCorrTree;
#    pragma link C++ class CellPropertyGenerator::DAC2MeVTree;
#    pragma link C++ class CellPropertyGenerator::DigitTree;
#    pragma link C++ class CellPropertyGenerator::AveragedDigitTree;
#    pragma link C++ class CellPropertyGenerator::LSBTree;
#    pragma link C++ class CellPropertyGenerator::PulseShapeTree;
#    pragma link C++ class CellPropertyGenerator::DirectGenerator;
#    pragma link C++ class CellPropertyGenerator::CaliWaveTree;
#    pragma link C++ class CellPropertyGenerator::PhysWaveTree;
#    pragma link C++ class CellPropertyGenerator::MPMCTree;
#    pragma link C++ class CellPropertyGenerator::CellPropertyTree;

#endif
