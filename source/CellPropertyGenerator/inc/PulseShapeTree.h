/// @addtogroup CellPropertyGenerator
/// @{

#pragma once
#include "Tools.h"
#include "Tree.h"
#include "TreeReader.h"

namespace CellPropertyGenerator
{
    /// Class to extract information from pulse shape tree and output higher level trees
    class PulseShapeTree : public TreeReader
    {
      public:
        /**
         * @brief Construct a new PulseShapeTree object
         * @param InputFileName Input pulse shape file name
         */
        PulseShapeTree(const std::string& InputFileName) :
            TreeReader(InputFileName, "PULSE"),
            channelId(m_reader, "channelId"),
            amplitude(m_reader, "amplitude"),
            time(m_reader, "time"),
            Nsamples(m_reader, "Nsamples"),
            maxAmp(m_reader, "maxAmp"),
            iMaxAmp(m_reader, "iMaxAmp"),
            minAmp(m_reader, "minAmp"),
            allCL(m_reader, "allCL"),
            pulsedCL(m_reader, "pulsedCL"),
            NpulsedCells(m_reader, "NpulsedCells"),
            DACforSC(m_reader, "DACforSC"),
            iCLset(m_reader, "iCLset"),
            iDAC(m_reader, "iDAC"),
            latomeSourceID(m_reader, "latomeSourceID"),
            layer(m_reader, "layer"),
            eta(m_reader, "eta"),
            phi(m_reader, "phi"),
            detector(m_reader, "detector"),
            ped(m_reader, "ped"),
            rms(m_reader, "rms"),
            DACtoMeV(m_reader, "DACtoMeV")
        {
            // gInterpreter->GenerateDictionary("std::vector<std::deque<double> >",
            // "vector;deque");
        }

        /// If pedestal has already been subtracted in the input pulse shape
        bool PedAlreadySubtracted{};
        /// Input LSB file name
        std::string LSBFileName;
        /// OFCs cannot be calibrated correctly if pulses in data start too late. In this case, you may cut the beginning of pulse in BC using this argument.
        int BCShift{};

        /// Generate cell property file
        void GenerateCellProperty();

        /// Output pulse shape plot in PDF
        void DrawPulseShape();

      private:
        TTreeReaderValue<Int_t> channelId;
        TTreeReaderValue<std::vector<Double_t>> amplitude;
        TTreeReaderValue<std::vector<Double_t>> time;
        TTreeReaderValue<Int_t> Nsamples;
        TTreeReaderValue<Float_t> maxAmp;
        TTreeReaderValue<Int_t> iMaxAmp;
        TTreeReaderValue<Float_t> minAmp;
        TTreeReaderValue<std::unordered_set<int>> allCL;
        TTreeReaderValue<std::unordered_set<int>> pulsedCL;
        TTreeReaderValue<Int_t> NpulsedCells;
        TTreeReaderValue<Int_t> DACforSC;
        TTreeReaderValue<Int_t> iCLset;
        TTreeReaderValue<Int_t> iDAC;
        TTreeReaderValue<Int_t> latomeSourceID;
        TTreeReaderValue<Int_t> layer;
        TTreeReaderValue<Float_t> eta;
        TTreeReaderValue<Float_t> phi;
        TTreeReaderValue<Int_t> detector;
        TTreeReaderValue<Double_t> ped;
        TTreeReaderValue<Double_t> rms;
        TTreeReaderValue<Double_t> DACtoMeV;
    };
} // namespace CellPropertyGenerator
  /// @}
