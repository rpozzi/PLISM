#include "DirectGenerator.h"
#include "AveragedDigitTree.h"
#include "DigitTree.h"
#include "LSBTree.h"
#include "PulsePattern.h"
#include "PulseShapeTree.h"
#include "Tools.h"

using namespace CellPropertyGenerator;

void DirectGenerator::GenerateFromAREUSPulseShapeTree(const std::string& PulseShapeFileName, const std::string& NoiseFileName, const std::string& OutputFileName)
{
    AREUSPulseShapeTree pst(PulseShapeFileName);
    pst.generate(NoiseFileName, OutputFileName);
}

void DirectGenerator::GenerateFromCalibFrameWork()
{
    PulsePattern PulsePatternObj(PatternFileName.c_str(), ID_SCID_CL_FileName.c_str());
    PulsePatternObj.OutputPattern();
    DigitTree* DigitTreeObj = new DigitTree(DigitFileName.c_str(), PulsePatternObj, ID_SCID_CL_FileName.c_str(), PedestalFileName.c_str(), DAC2MeVFileName.c_str(), NTriggers, NSamples);
    DigitTreeObj->MakeTree();
    delete DigitTreeObj;

    FileObject DigitTreeObj2(DigitFileName);

    AveragedDigitTree AveragedDigitTreeObj(DigitTreeObj2.AddPrefix("converted").string().c_str());
    AveragedDigitTreeObj.PulseShape();
    AveragedDigitTreeObj.PulseTree();
    AveragedDigitTreeObj.LSB_delayPeak();

    auto LSBTreeFileName{DigitTreeObj2.AddPrefix("lsb").string()};
    LSBTree LSBTreeObj(LSBTreeFileName);
    LSBTreeObj.LSBvsEta(Detector.c_str(), LSBReferenceFileName.c_str(), LSBPlotUpperLimit);

    PulseShapeTree PulseShapeTreeObj(DigitTreeObj2.AddPrefix("pulse").string());
    PulseShapeTreeObj.LSBFileName = LSBTreeFileName;
    PulseShapeTreeObj.BCShift     = BCShift;
    PulseShapeTreeObj.GenerateCellProperty();
}
