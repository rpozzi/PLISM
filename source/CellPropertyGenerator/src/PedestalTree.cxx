#include "PedestalTree.h"

using namespace CellPropertyGenerator;

std::unordered_map<Int_t, std::pair<Double_t, Double_t>> PedestalTree::getPed()
{
    std::unordered_map<Int_t, std::pair<Double_t, Double_t>> m;

    Restart();
    while (Next()) {
        m[*channelId] = std::make_pair(*ped, *rms);
    }
    return m;
}
