#include "PulsePattern.h"

using namespace CellPropertyGenerator;

void PulsePattern::OutputPattern()
{
    Message::Info("Pulse pattern " + m_patternFileName);
    Message::Info((boost::format("NDACs: %1%") % NDACs).str());
    Message::Info((boost::format("Ndelays: %1%") % Ndelays).str());
    Message::Info((boost::format("NCLsets: %1%") % NCLsets).str());
    Message::Info("DACs:");
    for (const auto& it : DACs) std::cout << it << " ";
    std::cout << "\n";
    Message::Info("delays:");
    for (const auto& it : delays) std::cout << it << " ";
    std::cout << "\n";
    Message::Info("CLsets:");
    for (const auto& set_it : CLsets) {
        std::set<int> s;
        for (const auto& it : set_it) s.insert(it);
        for (const auto& it : s) std::cout << it << " ";
        std::cout << "\n";
    }
    Message::Info("CLs without pulse:");
    for (const auto& it : notPulsedCLs) std::cout << it << " ";
    std::cout << "\n";

    if (m_ID_SCID_CL_fileName.empty()) return;

    Message::Info("CLs not in the region:");
    for (const auto& it : outofRegionCLs) std::cout << it << " ";
    std::cout << "\n";
    Message::Info("CLs not pulsed in the region:");
    for (const auto& it : notPulsedInsideCLs) std::cout << it << " ";
    std::cout << "\n";
    Message::Info("Cells without any pulsed CLs in the region:");
    for (const auto& it : notPulsedInsideCellIDs) std::cout << it << " ";
    std::cout << "\n";
}

std::unordered_set<int> PulsePattern::getPulsedCL(std::array<uint32_t, 4>& a)
{
    std::unordered_set<int> v;
    for (int i = 0; i < 32; ++i)
        if (a.at(0) & (1 << i)) v.insert(i);
    for (int i = 0; i < 32; ++i)
        if (a.at(1) & (1 << i)) v.insert(i + 32);
    for (int i = 0; i < 32; ++i)
        if (a.at(2) & (1 << i)) v.insert(i + 64);
    for (int i = 0; i < 32; ++i)
        if (a.at(3) & (1 << i)) v.insert(i + 96);
    return v;
}

void PulsePattern::Initialize()
{
    Message::Info("PulsePattern Initialize");
    std::ifstream ifs(m_patternFileName);
    if (not ifs.is_open()) Message::Fatal(m_patternFileName + " cannot be opened");
    ifs >> NDACs;
    DACs.resize(NDACs);
    for (int i = 0; i < NDACs; ++i) ifs >> DACs[i];
    ifs >> Ndelays;
    delays.resize(Ndelays);
    for (int i = 0; i < Ndelays; ++i) ifs >> delays[i];
    ifs >> NCLsets;
    CLsets.reserve(NCLsets);
    std::array<std::string, 4> CLset_str;
    std::array<uint32_t, 4> CLset;
    std::string tmp;
    while (getline(ifs, tmp)) {
        // deleteSpace(tmp);
        if (tmp.empty()) continue;
        std::stringstream ss;
        ss << tmp;
        ss >> CLset_str[0] >> CLset_str[1] >> CLset_str[2] >> CLset_str[3];
        for (int i = 0; i < 4; ++i) CLset[i] = static_cast<uint32_t>(std::strtol(CLset_str[i].c_str(), NULL, 16));
        CLsets.push_back(getPulsedCL(CLset));
    }

    if (static_cast<int>(DACs.size()) != NDACs) Message::Fatal("DACs.size() != NDACs");
    if (static_cast<int>(delays.size()) != Ndelays) Message::Fatal("delays.size() != Ndelays");
    if (static_cast<int>(CLsets.size()) != NCLsets) Message::Fatal("CLsets.size() != NCLsets");

    for (int i = 0; i < 128; ++i) {
        bool found = false;
        for (const auto& set_it : CLsets) {
            if (set_it.find(i) != set_it.end()) {
                found = true;
                break;
            }
        }
        if (!found) notPulsedCLs.insert(i);
    }
}

void PulsePattern::setCellInfo()
{
    int cellID, SCID, CL;
    std::ifstream ifs_map(m_ID_SCID_CL_fileName);
    std::string line;
    while (std::getline(ifs_map, line)) {
        std::istringstream iss(line);
        if (not(iss >> cellID >> SCID)) Message::Fatal("Unexpected format of ID_SCID_CL_fileName");
        while (iss >> CL) {
            if (SCID) {
                CLset_fromDB.insert(CL);
                cellID_CLs_fromDB[cellID].push_back(CL);
            }
        }
    }
    for (int i = 0; i < 128; ++i) {
        if (CLset_fromDB.find(i) != CLset_fromDB.end()) continue;
        outofRegionCLs.insert(i);
    }
    for (const auto& cl : CLset_fromDB) {
        if (notPulsedCLs.find(cl) == notPulsedCLs.end()) continue;
        notPulsedInsideCLs.insert(cl);
    }
    for (const auto& ch : cellID_CLs_fromDB) {
        bool pulsed = false;
        for (const auto& cl : ch.second) {
            if (notPulsedCLs.find(cl) == notPulsedCLs.end()) {
                pulsed = true;
                break;
            }
        }
        if (!pulsed) notPulsedInsideCellIDs.insert(ch.first);
    }
}
