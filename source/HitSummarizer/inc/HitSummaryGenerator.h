/// @addtogroup HitSummarizer
/// @{

#pragma once
#include <TInterpreter.h>
#include <TMath.h>
#include <TRandom.h>
#include <iostream>
#include "MCHitSummaryGenerator.h"

namespace HitSummarizer
{
    /// Generator class of general hit summary files
    class HitSummaryGenerator
    {
      public:
        ///
        /// @brief Constructor for pileup hit summary file generation
        /// @param inputFileName Input HITS file name
        ///
        HitSummaryGenerator(const std::string& inputFileName) :
            InputFileName(inputFileName) {}

        /// Constructor for signal hit summary file generation
        HitSummaryGenerator(){};

        void summarize();  ///< Generate pileup hit summary file
        void makeSignal(); ///< Generate signal hit summary file

        std::string InputFileName;        ///< Input HITS file name
        std::string OutputFileName;       ///< Output hit summary file name
        std::string CellPropertyFileName; ///< Input cell property file name
        std::string LITFileName;          ///< Input LAr ID translator file name
        int NEvent{-1};                   ///< Number of signal events to be generated
        float EtMin{-1.};                 ///< Lower bound of input signal Et range
        float EtMax{-1.};                 ///< Upper bound of input signal Et range
        float TauMin{99999.};             ///< Lower bound of input signal tau range
        float TauMax{99999.};             ///< Upper bound of input signal tau range
        int seed{1};                      ///< Seed of random function to be used to get Et and tau for each event

      private:
        inline void checkSeed()
        {
            Message::Info((boost::format("Seed is set as %1% for Hitsummarizer") % seed).str());
            if (seed == 1)
                Message::Warning("The seed is set as 1 which is the default value. You might have forgotten to specify a seed.");
            else if (not seed)
                Message::Warning("This seed may cause inconsistency of hits in each event when you split HitSummary with respect to cell/region, because the seed is not reset appropriately for each cell/region. Seed is recommended to be an integer larger than 0.");
            else if (seed < 0)
                Message::Fatal("Negative seed cannot be used. An integer larger than 0 is recommended.");
        }
    };
} // namespace HitSummarizer
/// @}
