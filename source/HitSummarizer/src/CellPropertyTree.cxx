#include "CellPropertyTree.h"

using namespace HitSummarizer;

std::vector<UInt_t> CellPropertyTree::getChannelIDs()
{
    std::vector<UInt_t> v;

    Restart();
    while (Next()) {
        v.push_back(*channelId);
    }
    sort(v.begin(), v.end());
    v.erase(unique(v.begin(), v.end()), v.end());
    return v;
}
