#include "HitSummaryGenerator.h"
#include <TVector2.h>
#include "CellPropertyTree.h"

using namespace HitSummarizer;

void HitSummaryGenerator::summarize()
{
    if (InputFileName.empty()) Message::Fatal("Undefined InputFileName");
    if (OutputFileName.empty()) Message::Fatal("Undefined OutputFileName");
    if (LITFileName.empty()) Message::Fatal("Undefined LITFileName");
    MCHitSummaryGenerator a(InputFileName.c_str());
    a.OutputFileName = OutputFileName;
    a.LITFileName    = LITFileName;
    a.summarize();
}

void HitSummaryGenerator::makeSignal()
{
    checkSeed();
    gRandom->SetSeed(seed);
    if (OutputFileName.empty()) Message::Fatal("Undefined OutputFileName");
    if (CellPropertyFileName.empty()) Message::Fatal("Undefined CellPropertyFileName");
    if (NEvent < 0) Message::Fatal("Undefined NEvent");
    if (EtMin < 0) Message::Fatal("Undefined EtMin");
    if (EtMax < 0) Message::Fatal("Undefined EtMax");
    if (TauMin == 99999.) Message::Fatal("Undefined TauMin");
    if (TauMax == 99999.) Message::Fatal("Undefined TauMax");

    CellPropertyTree CellPropertyTree(CellPropertyFileName.c_str());
    const auto channelIDs{CellPropertyTree.getChannelIDs()};

    gInterpreter->GenerateDictionary("vector<vector<float> >", "vector");

    const auto fg{FileGenerator(OutputFileName)};
    TFile* outputFile = new TFile(OutputFileName.c_str(), "recreate");
    TTree* outputTree = new TTree("HitSummary", "HitSummary");

    Int_t channelId;
    std::vector<std::vector<float> > ET;
    std::vector<std::vector<float> > delay;

    outputTree->Branch("channelId", &channelId);
    outputTree->Branch("ET", &ET);
    outputTree->Branch("delay", &delay);

    if (EtMin == EtMax) {
        for (int i = 0; i < NEvent; ++i) ET.push_back({EtMin});
    } else {
        if (EtMin) {
            for (int i = 0; i < NEvent; ++i)
                ET.push_back(
                  {static_cast<float>(gRandom->Uniform(EtMin, EtMax))});
        } else {
            for (int i = 0; i < NEvent; ++i)
                ET.push_back({static_cast<float>(gRandom->Uniform(EtMax))});
        }
    }
    if (TauMin == TauMax) {
        for (int i = 0; i < NEvent; ++i) delay.push_back({TauMin});
    } else {
        if (TauMin) {
            for (int i = 0; i < NEvent; ++i)
                delay.push_back(
                  {static_cast<float>(gRandom->Uniform(TauMin, TauMax))});
        } else {
            for (int i = 0; i < NEvent; ++i)
                delay.push_back({static_cast<float>(gRandom->Uniform(TauMax))});
        }
    }

    for (const auto& it : channelIDs) {
        channelId = it;
        outputTree->Fill();
    }

    outputTree->Write();
    outputFile->Close();
}
