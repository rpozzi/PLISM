#include "MCHitSummaryGenerator.h"
#include <TVector2.h>
#include "CellPropertyTree.h"

#define ENABLE_EMB 1
#define ENABLE_EMEC 1
#define ENABLE_FCAL 0
#define ENABLE_HEC 0

using namespace HitSummarizer;

void MCHitSummaryGenerator::summarize()
{
    if (not m_configured) Message::Fatal("Used constructor was wrong. Correct one is HitSummaryGenerator(const std::string& inputFileName)");

    if (LITFileName.empty()) Message::Fatal("Undefined LITFileName");
    LArIDtranslator LIT(LITFileName.c_str());
    const auto LITMap{LIT.GetUM4HitSummary()};

    typedef std::vector<float> Hits;
    typedef std::vector<Hits> Events;
    typedef std::pair<Events, Events> EtTime;
    std::unordered_map<Int_t, EtTime> EtTimeMap;

    int nentries{};
    Restart();
    while (Next()) ++nentries;

    Restart();
    int jentry{-1};
    while (Next()) {
        ++jentry;

        Message::Info((boost::format("Processing event %1% ...") % jentry).str());

        if (not((ENABLE_EMB and *hitemb_n) or (ENABLE_EMEC and *hitemec_n) or (ENABLE_FCAL and *hitfcal_n) or (ENABLE_HEC and *hithec_n))) {
            Message::Debug("empty");
            continue;
        }

        auto iterate = [nentries, jentry, &EtTimeMap, &LITMap](
                         const std::size_t& nhits, const std::vector<float>& veta, const std::vector<float>& vphi, const std::vector<float>& vE, const std::vector<float>& vTime, const std::vector<unsigned int>& vID) {
            if (nhits not_eq veta.size() or nhits not_eq vphi.size() or nhits not_eq vE.size() or nhits not_eq vTime.size() or nhits not_eq vID.size()) {
                Message::Fatal("Number of hits in a event is not consistent! Use another hit file.");
            }
            auto eta  = std::begin(veta);
            auto phi  = std::begin(vphi);
            auto E    = std::begin(vE);
            auto Time = std::begin(vTime);
            auto ID   = std::begin(vID);
            for (; eta not_eq std::end(veta);
                 ++eta, ++phi, ++E, ++Time,
                 ++ID) { // for (position, energy, time) in the event
                auto keyID = LITMap.find(*ID);
                if (keyID == LITMap.end()) {
                    Message::Debug((boost::format("ID %1% is not listed in the LArIDtranslator object") % *ID).str());
                    continue;
                }
                Int_t theSC{};
                {
                    const auto& v{keyID->second.v};
                    const auto Etawindow{keyID->second.Etawindow};
                    const auto Phiwindow{keyID->second.Phiwindow};
                    Double_t min{99999.};
                    bool not_yet{true};
                    for (const auto& st : v) {
                        const auto etadiff{st.ETA - *eta};
                        if (not_yet) {
                            if (etadiff >= -Etawindow)
                                not_yet = false;
                            else
                                continue;
                        }
                        if (etadiff > Etawindow) break;
                        const auto phidiff{st.PHI - *phi};
                        if (phidiff >= -Phiwindow and phidiff <= Phiwindow) {
                            const auto deta{st.ETA - *eta};
                            const auto dphi{TVector2::Phi_mpi_pi(phidiff)};
                            auto DR2{deta * deta + dphi * dphi};
                            if (DR2 < min) {
                                min   = DR2;
                                theSC = st.SCID;
                            }
                        }
                    }
                }

                auto isc = EtTimeMap.find(theSC); // find channelId in EtTimeMap
                if (isc == EtTimeMap.end()) {
                    Message::Verbose((boost::format("New channelId: %1%") % theSC).str());
                    Events v_tmp;
                    v_tmp.resize(nentries);
                    auto pair_tmp{std::make_pair(v_tmp, v_tmp)};
                    pair_tmp.first[jentry]  = {static_cast<float>(
                      *E * TMath::Sin(2 * TMath::ATan(TMath::Exp(-*eta))))};
                    pair_tmp.second[jentry] = {*Time};
                    EtTimeMap[theSC]        = std::move(pair_tmp);
                } else {
                    (isc->second.first)[jentry].push_back(static_cast<float>(
                      *E * TMath::Sin(2 * TMath::ATan(TMath::Exp(-*eta)))));
                    (isc->second.second)[jentry].push_back(*Time);
                }
            }
        };
        if (ENABLE_EMB and *hitemb_n)
            iterate(*hitemb_n, *hitemb_eta, *hitemb_phi, *hitemb_E, *hitemb_Time, *hitemb_ID);
        if (ENABLE_EMEC and *hitemec_n)
            iterate(*hitemec_n, *hitemec_eta, *hitemec_phi, *hitemec_E, *hitemec_Time, *hitemec_ID);
        if (ENABLE_FCAL and *hitfcal_n)
            iterate(*hitfcal_n, *hitfcal_eta, *hitfcal_phi, *hitfcal_E, *hitfcal_Time, *hitfcal_ID);
        if (ENABLE_HEC and *hithec_n)
            iterate(*hithec_n, *hithec_eta, *hithec_phi, *hithec_E, *hithec_Time, *hithec_ID);
    }

    {
        gInterpreter->GenerateDictionary("vector<vector<float> >", "vector");

        if (OutputFileName.empty()) Message::Fatal("Undefined OutputFileName");
        const auto fg{FileGenerator(OutputFileName)};
        TFile* outputFile = new TFile(OutputFileName.c_str(), "recreate");
        TTree* outputTree = new TTree("HitSummary", "HitSummary");

        Int_t channelId;
        std::vector<std::vector<float> > ET;    // events<hits<et>>
        std::vector<std::vector<float> > delay; // events<hits<delay>>

        outputTree->Branch("channelId", &channelId);
        outputTree->Branch("ET", &ET);
        outputTree->Branch("delay", &delay);

        for (auto&& scid : EtTimeMap) {
            channelId = scid.first;
            ET        = scid.second.first;
            delay     = scid.second.second;
            outputTree->Fill();
        }

        outputTree->Write();
        outputFile->Close();
    }
}
