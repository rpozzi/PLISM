/// @addtogroup Reconstructor
/// @{

#pragma once
#include "TreeReader.h"

namespace Reconstructor
{
    /// Class to extract information from PLISM OFC tree
    class OFCTree : public TreeReader
    {
      public:
        /// Structure containing OFC information
        struct OFC {
            std::vector<double> a; ///< OFC a vector per phase
            std::vector<double> b; ///< OFC b vector per phase
            Int_t OFBCshift;       ///< Which BC (starting from 0) to start from and perform optimal filtering
        };
        /// channelId vs @ref OFC map
        typedef std::unordered_map<unsigned int, OFC> OFCMap;

        /**
         * @brief Construct a new OFCTree object
         *
         * @param inputFileName Input PLISM OFC file name
         * @param inputTreeName Input PLISM OFC tree name
         */
        OFCTree(const std::string& inputFileName, const std::string& inputTreeName) :
            TreeReader(inputFileName, inputTreeName),
            channelId(m_reader, "channelId"),
            a(m_reader, "a"),
            b(m_reader, "b"),
            OFBCshift(m_reader, "OFBCshift") {}

        OFCMap FillOFCMap(); ///< Return filled @ref OFCMap

      private:
        TTreeReaderValue<Int_t> channelId;
        TTreeReaderValue<std::vector<double>> a;
        TTreeReaderValue<std::vector<double>> b;
        TTreeReaderValue<Int_t> OFBCshift;
    };
} // namespace Reconstructor
  /// @}
