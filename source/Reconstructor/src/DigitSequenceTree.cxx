#include "DigitSequenceTree.h"
#include <iostream>

using namespace Reconstructor;

void DigitSequenceTree::reconstruct()
{
    try {
        if (OutputFileName == "") throw "Undefined OutputFileName";
        if (OFCFileName == "") throw "Undefined OutputFileName";
        if (fChain == 0) throw "fChain == 0";

        OFCTree obj(OFCFileName.c_str(), "OFC");
        if (obj())
            m_OFCMap = obj.FillOFCMap();
        else
            throw "OFCTree object not configured";

        const auto fg{FileGenerator(OutputFileName)};
        TFile* outputFile = new TFile(OutputFileName.c_str(), "recreate");
        TTree* outputTree = new TTree("reconstructed", "reconstructed");
        
        std::vector<double> OFCa;
        std::vector<double> OFCb;
        Int_t OFBCshift;
        std::vector<double> SignalTrueEtTau;
        std::vector<double> RecEt;
        std::vector<double> RecTau;
        std::vector<double> RecEtTau;
        std::vector<double> RecSignalAnalogEt;
        std::vector<double> RecSignalAnalogTau;
        std::vector<double> RecSignalAnalogEtTau;

        std::vector<std::vector<double>> emptyVV_d;
        std::vector<double> emptyVector_d;
        std::deque<double> emptyDeque_d;
        std::deque<int> emptyDeque_i;
        
        outputTree->Branch("channelId", &channelId, "channelId/I");
        outputTree->Branch("eta", &eta, "eta/F");
        outputTree->Branch("phi", &phi, "phi/F");
        outputTree->Branch("layer", &layer, "layer/I");
        outputTree->Branch("detector", &detector, "detector/I");
        outputTree->Branch("LSB", &LSB, "LSB/F");
        outputTree->Branch("pedADC", &pedADC, "pedADC/D");
        outputTree->Branch("noise", &noise, "noise/F");
        outputTree->Branch("OFCa", &OFCa);
        outputTree->Branch("OFCb", &OFCb);
        outputTree->Branch("OFBCshift", &OFBCshift, "OFBCshift/I");
        outputTree->Branch("NBC", &NBC, "NBC/I");
        outputTree->Branch("phase", &phase, "phase/F");
        outputTree->Branch("mu", &mu, "mu/D");
        outputTree->Branch("TrainPattern", TrainPattern);
        outputTree->Branch("SignalPattern", SignalPattern);
        outputTree->Branch("SignalTrueEt", OutputBranch_SignalTrueEt ? SignalTrueEt : &emptyVector_d);
        outputTree->Branch("SignalTrueTau", OutputBranch_SignalTrueTau ? SignalTrueTau : &emptyVector_d);
        outputTree->Branch("SignalTrueEtTau", &SignalTrueEtTau);
        outputTree->Branch("PileupTrueEt",
                           OutputBranch_PileupTrueEt ? PileupTrueEt : &emptyVV_d);
        outputTree->Branch("PileupTrueEtSum", OutputBranch_PileupTrueEtSum ? PileupTrueEtSum : &emptyVector_d);
        outputTree->Branch("PileupTrueTau",
                           OutputBranch_PileupTrueTau ? PileupTrueTau : &emptyVV_d);
        outputTree->Branch("AnalogEt",
                           OutputBranch_AnalogEt ? AnalogEt : &emptyDeque_d);
        outputTree->Branch("SignalAnalogEt", OutputBranch_SignalAnalogEt ? SignalAnalogEt : &emptyDeque_d);
        outputTree->Branch("BgAnalogEt",
                           OutputBranch_BgAnalogEt ? BgAnalogEt : &emptyDeque_d);
        outputTree->Branch("PileupAnalogEt", OutputBranch_PileupAnalogEt ? PileupAnalogEt : &emptyDeque_d);
        outputTree->Branch("ElecNoiseAnalogEt", OutputBranch_ElecNoiseAnalogEt ? ElecNoiseAnalogEt : &emptyDeque_d);
        outputTree->Branch("Digit", OutputBranch_Digit ? Digit : &emptyDeque_i);
        outputTree->Branch("RecEt", &RecEt);
        outputTree->Branch("RecTau", &RecTau);
        outputTree->Branch("RecEtTau", &RecEtTau);
        outputTree->Branch("RecSignalAnalogEt", &RecSignalAnalogEt);
        outputTree->Branch("RecSignalAnalogTau", &RecSignalAnalogTau);
        outputTree->Branch("RecSignalAnalogEtTau", &RecSignalAnalogEtTau);

        Long64_t nentries{fChain->GetEntriesFast()};
        Long64_t nbytes{}, nb{};
        for (Long64_t jentry{}; jentry < nentries; ++jentry) {
            Long64_t ientry{LoadTree(jentry)};
            if (ientry < 0) break;
            nb = fChain->GetEntry(jentry);
            nbytes += nb;
            
            if (OutputBranch_SignalTrueEtTau and static_cast<Int_t>(SignalTrueEt->size()) >= NBC) {
                std::vector<double> SignalTrueEtTau_tmp;
                SignalTrueEtTau_tmp.reserve(NBC);
                for (int i{}; i < NBC; ++i)
                    SignalTrueEtTau_tmp.push_back(SignalTrueEt->at(i) * SignalTrueTau->at(i));
                SignalTrueEtTau = std::move(SignalTrueEtTau_tmp);
            }

            auto foundSC{m_OFCMap.find(channelId)};
            if (foundSC == m_OFCMap.end()) {
                Message::Warning((boost::format("There are no OFCs for channelId %1%") % channelId).str());
                continue;
            }
            OFCa      = foundSC->second.a;
            OFCb      = foundSC->second.b;
            OFBCshift = foundSC->second.OFBCshift;

            const int nSamps{static_cast<int>(OFCa.size())};
            if (nSamps != static_cast<int>(OFCb.size()))
                throw "Size of OFC a and b are not consistent!\nAborted.";
            const int nIterate{NBC - nSamps + 1 - OFBCshift};
            { // digit iteration
                const auto& digi{*Digit};
                if (digi.size() and (OutputBranch_RecEt or OutputBranch_RecTau or OutputBranch_RecEtTau)) {
                    std::vector<double> Et_tmp;
                    std::vector<double> Tau_tmp;
                    std::vector<double> EtTau_tmp;
                    Et_tmp.reserve(nIterate);
                    Tau_tmp.reserve(nIterate);
                    EtTau_tmp.reserve(nIterate);
                    for (int i = 0; i < nIterate; ++i) {
                        double ET{}, tau{};
                        for (int j{}; j < nSamps; ++j) {
                            ET += OFCa[j] * digi[i + OFBCshift + j];
                            tau += OFCb[j] * digi[i + OFBCshift + j];
                        }
                        EtTau_tmp.push_back(tau);
                        tau /= ET;
                        Et_tmp.push_back(ET);
                        Tau_tmp.push_back(tau);
                    }
                    if (OutputBranch_RecEt) RecEt = std::move(Et_tmp);
                    if (OutputBranch_RecTau) RecTau = std::move(Tau_tmp);
                    if (OutputBranch_RecEtTau) RecEtTau = std::move(EtTau_tmp);
                }
            }
            { // SignalAnalogEt iteration
                const auto& sig{*SignalAnalogEt};
                if (sig.size() and (OutputBranch_RecSignalAnalogEt or OutputBranch_RecSignalAnalogTau or OutputBranch_RecSignalAnalogEtTau)) {
                    std::vector<double> Et_tmp;
                    std::vector<double> Tau_tmp;
                    std::vector<double> EtTau_tmp;
                    Et_tmp.reserve(nIterate);
                    Tau_tmp.reserve(nIterate);
                    EtTau_tmp.reserve(nIterate);
                    for (int i = 0; i < nIterate; ++i) {
                        double ET{}, tau{};
                        for (int j{}; j < nSamps; ++j) {
                            ET += OFCa[j] * sig[i + OFBCshift + j] / LSB;
                            tau += OFCb[j] * sig[i + OFBCshift + j] / LSB;
                        }
                        EtTau_tmp.push_back(tau);
                        tau /= ET;
                        Et_tmp.push_back(ET);
                        Tau_tmp.push_back(tau);
                    }
                    if (OutputBranch_RecSignalAnalogEt) RecSignalAnalogEt = std::move(Et_tmp);
                    if (OutputBranch_RecSignalAnalogTau)
                        RecSignalAnalogTau = std::move(Tau_tmp);
                    if (OutputBranch_RecSignalAnalogEtTau)
                        RecSignalAnalogEtTau = std::move(EtTau_tmp);
                }
            }

            outputTree->Fill();
        }

        outputTree->Write();
        outputFile->Close();

    } catch (const std::string& str) {
        Message::Fatal(str);
    }
}
