/// @addtogroup SCconditionSimulator
/// @{

#pragma once
#include "Tree.h"

namespace SCconditionSimulator
{
    /// Class to extract information from reconstructed tree and cellproperty tree
    class ReconstructedTree : public Tree
    {
      public:
        /**
         * @brief Construct a new ReconstructedTree object
         *
         * @param inputFileName Input reconstucted file name
         */

        ReconstructedTree(const std::string& inputFileName) :
            Tree(inputFileName, "reconstructed")
        {
            TrainPattern      = 0;
            SignalPattern     = 0;
            ElecNoiseAnalogEt = 0;
            OFCa              = 0;
            OFCb              = 0;
            SignalTrueEt      = 0;
            SignalTrueTau     = 0;
            SignalTrueEtTau   = 0;
            PileupTrueEt      = 0;
            PileupTrueTau     = 0;
            RecEt             = 0;
            RecTau            = 0;
            RecEtTau          = 0;

            fChain->SetBranchAddress("channelId", &channelId, &b_channelId);
            fChain->SetBranchAddress("eta", &eta, &b_eta);
            fChain->SetBranchAddress("phi", &phi, &b_phi);
            fChain->SetBranchAddress("layer", &layer, &b_layer);
            fChain->SetBranchAddress("detector", &detector, &b_detector);
            fChain->SetBranchAddress("LSB", &LSB, &b_LSB);
            fChain->SetBranchAddress("pedADC", &pedADC, &b_pedADC);
            fChain->SetBranchAddress("noise", &noise, &b_noise);
            fChain->SetBranchAddress("NBC", &NBC, &b_NBC);
            fChain->SetBranchAddress("phase", &phase, &b_phase);
            fChain->SetBranchAddress("mu", &mu, &b_mu);

            fChain->SetBranchAddress("TrainPattern", &TrainPattern, &b_TrainPattern);
            fChain->SetBranchAddress("SignalPattern", &SignalPattern, &b_SignalPattern);
            fChain->SetBranchAddress("ElecNoiseAnalogEt", &ElecNoiseAnalogEt, &b_ElecNoiseAnalogEt);
            fChain->SetBranchAddress("OFCa", &OFCa, &b_OFCa);
            fChain->SetBranchAddress("OFCb", &OFCb, &b_OFCb);
            fChain->SetBranchAddress("SignalTrueEt", &SignalTrueEt, &b_SignalTrueEt);
            fChain->SetBranchAddress("SignalTrueTau", &SignalTrueTau, &b_SignalTrueTau);
            fChain->SetBranchAddress("SignalTrueEtTau", &SignalTrueEtTau, &b_SignalTrueEtTau);
            fChain->SetBranchAddress("PileupTrueEt", &PileupTrueEt, &b_PileupTrueEt);
            fChain->SetBranchAddress("PileupTrueTau", &PileupTrueTau, &b_PileupTrueTau);
            fChain->SetBranchAddress("RecEt", &RecEt, &b_RecEt);
            fChain->SetBranchAddress("RecTau", &RecTau, &b_RecTau);
            fChain->SetBranchAddress("RecEtTau", &RecEtTau, &b_RecEtTau);
        }

        std::string OutputFileName;       ///< Name of RecoAccuracyTree file to be output
        std::string inputCellpropFileName;       ///< Name of CellProperty Tree file to be input
        void RecoAccuracyTree();        ///< Generate RecoAccuracy Tree file. There is no argument.

        bool OutputBranch_SignalTrueEt{true};      ///< Whether to fill SignalTrueEt branch
        bool OutputBranch_SignalTrueTau{true};     ///< Whether to fill SignalTrueTau branch
        bool OutputBranch_SignalTrueEtTau{true};     ///< Whether to fill SignalTrueEtTau branch
        bool OutputBranch_RecTau{true};     ///< Whether to fill RecTau branch
        bool OutputBranch_RecEt{true};     ///< Whether to fill RecEt branch
        bool OutputBranch_RecEtTau{true};     ///< Whether to fill RecEtTau branch
        bool OutputBranch_ReldiffRecTrueEt{true};     ///< Whether to fill ReldiffRecTrueEt branch
        bool OutputBranch_PileupTrueEt{true};     ///< Whether to fill PileupTrueEt branch
        bool OutputBranch_PileupTrueTau{true};     ///< Whether to fill PuleupTrueTau branch
        bool OutputBranch_MeanRecEt{true};     ///< Whether to fill MeanRecEt branch
        bool OutputBranch_MeanRecTau{true};     ///< Whether to fill MeanRecTau branch
        bool OutputBranch_MeanRecEtTau{true};     ///< Whether to fill MeanRecEtTau branch
        bool OutputBranch_MeanReldiffRecTrueEt{true};     ///< Whether to fill MeanRecEtTau branch
        bool OutputBranch_RMSRecEt{true};     ///< Whether to fill RMSRecEt branch
        bool OutputBranch_RMSRecTau{true};     ///< Whether to fill RMSRecTau branch
        bool OutputBranch_RMSRecEtTau{true};     ///< Whether to fill RMSRecEtTau branch
        bool OutputBranch_RMSReldiffRecTrueEt{true};     ///< Whether to fill RMSRecTrueEt branch

      private:
        Int_t channelId;
        Float_t eta;
        Float_t phi;
        Int_t layer;
        Int_t detector;
        Float_t LSB;
        Double_t pedADC;
        Float_t noise;
        Int_t NBC;
        Float_t phase;
        Double_t mu;
        std::vector<int> *TrainPattern;
        std::vector<int> *SignalPattern;
        std::deque<double> *ElecNoiseAnalogEt;
        std::vector<double> *OFCa;
        std::vector<double> *OFCb;
        std::vector<double> *SignalTrueEt;
        std::vector<double> *SignalTrueTau;
        std::vector<double> *SignalTrueEtTau;
        std::vector<double> *PileupTrueEt;
        std::vector<double> *PileupTrueTau;
        std::vector<double> *RecEt;
        std::vector<double> *RecTau;
        std::vector<double> *RecEtTau;

        TBranch *b_channelId;
        TBranch *b_eta;
        TBranch *b_phi;
        TBranch *b_layer;
        TBranch *b_detector;
        TBranch *b_LSB;
        TBranch *b_pedADC;
        TBranch *b_noise;
        TBranch *b_NBC;
        TBranch *b_phase;
        TBranch *b_mu;
        TBranch *b_TrainPattern;
        TBranch *b_SignalPattern;
        TBranch *b_ElecNoiseAnalogEt;
        TBranch *b_OFCa;
        TBranch *b_OFCb;
        TBranch *b_SignalTrueEt;
        TBranch *b_SignalTrueTau;
        TBranch *b_SignalTrueEtTau;
        TBranch *b_PileupTrueEt;
        TBranch *b_PileupTrueTau;
        TBranch *b_RecEt;
        TBranch *b_RecTau;
        TBranch *b_RecEtTau;
    };
} // SCconditionSimulater
  /// @}
