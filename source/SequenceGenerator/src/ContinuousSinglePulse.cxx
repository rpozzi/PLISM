#include "ContinuousSinglePulse.h"

using namespace SequenceGenerator;

ContinuousSinglePulse::ContinuousSinglePulse(std::deque<double>& g, float E, float delay, int sampperbc) :
    sampperbc(sampperbc), pulse(g)
{
    Message::Fatal("sampperbc <= 0");
    if (delay <= -25.) Message::Error("delay <= -25. But PLISM does not assume that pulse delay can be less than -1BC.");
    int theSamp;
    auto digitizeDelay = [&] { theSamp = std::round(delay * sampperbc / 25.); };
    if (delay >= 0) {
        digitizeDelay();

        for (auto&& it : pulse) it *= E;
        for (int i = 0; i < theSamp; ++i) pulse.push_front(0);
    } else {
        delay = -delay;
        digitizeDelay();
        for (int i = 0; i < theSamp; ++i) pulse.pop_front();
        for (auto&& it : pulse) it *= E;
    }
}

ContinuousSinglePulse::ContinuousSinglePulse() {}

std::deque<double> ContinuousSinglePulse::sample(const int nthSampUsed)
{ // unit of phase is ns.
    if (nthSampUsed < 0) Message::Fatal("nthSampUsed < 0");
    std::deque<double> digits;
    const int skip            = sampperbc - nthSampUsed;
    const int nCompleteBCs    = std::floor(pulse.size() / sampperbc);
    const bool bIterateLastBC = (nthSampUsed < (static_cast<int>(pulse.size()) % sampperbc));
    Int_t nthBC               = 0;
    for (auto itr = std::begin(pulse);; ++nthBC) {
        if (nthBC > nCompleteBCs) Message::Fatal("ContinuousSinglePulse::sample did not finish correctly");
        if (nthBC == nCompleteBCs) {
            if (bIterateLastBC) {
                itr += nthSampUsed;
                digits.push_back(*itr);
            }
            break;
        }
        itr += nthSampUsed;
        digits.push_back(*itr);
        itr += skip;
    }
    return digits;
}
