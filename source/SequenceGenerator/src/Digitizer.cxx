#include "Digitizer.h"

using namespace SequenceGenerator;

void Digitizer::sequence()
{
    checkSeed();
    gRandom->SetSeed(seed);
    memberCheck();

    setCellPropertyMaps();

    // Signal
    SequencesDataMap signalSequenceMap{};
    {
        int SignalPatternSize{static_cast<int>(SignalPattern.size())};
        for (int i{}; i < SignalPatternSize; ++ ++i) {
            if (SignalPattern[i]) {
                m_withSignal = true;
                break;
            }
        }
        if (m_withSignal) {
            HitSummaryTree signalHSTree(SignalHitSummaryFile.c_str());
            std::vector<bool> signalInteractionPattern{getSignalInteractionPattern()};
            signalSequenceMap = getSequencesMap(signalHSTree.HitSummaryDataMapVar,
                                                std::move(signalInteractionPattern));
        }
    }

    // Pileup
    SequenceDataMap lowPtPileupSequenceMap{};
    SequenceDataMap highPtPileupSequenceMap{};
    std::unordered_map<Int_t, std::vector<std::vector<double>>> PileupTrueEtMap;
    std::unordered_map<Int_t, std::vector<std::vector<double>>> PileupTrueTauMap;
    std::unordered_map<Int_t, std::vector<double>> PileupTrueEtSumMap;
    {
        int TrainPatternSize{static_cast<int>(TrainPattern.size())};
        for (int i{}; i < TrainPatternSize; ++ ++i) {
            if (TrainPattern[i]) {
                m_withPileup = true;
                break;
            }
        }
        if (m_withPileup) {
            HitSummaryTree lowPtPileupHSTree(LowPtPileupHitSummaryFile.c_str());
            HitSummaryTree highPtPileupHSTree(HighPtPileupHitSummaryFile.c_str());
            auto pair_highlowPtNInteraction{getPileupNInteraction(PROP_HIGH)};
            const auto& highPtNInteraction = pair_highlowPtNInteraction.first;
            const auto& lowPtNInteraction  = pair_highlowPtNInteraction.second;

            lowPtPileupSequenceMap = getSequenceMap(
              lowPtPileupHSTree.HitSummaryDataMapVar, std::move(lowPtNInteraction));
            auto lowPtPileupTrueEtMap{std::move(ETOut)};
            auto lowPtPileupTrueTauMap{std::move(delayOut)};

            highPtPileupSequenceMap = getSequenceMap(highPtPileupHSTree.HitSummaryDataMapVar,
                                                     std::move(highPtNInteraction));
            auto highPtPileupTrueEtMap{std::move(ETOut)};
            auto highPtPileupTrueTauMap{std::move(delayOut)};

            if (OutputBranch_PileupTrueEt or OutputBranch_PileupTrueEtSum) {
                for (auto&& it : lowPtPileupTrueEtMap) {
                    const auto chid{it.first};
                    auto& vvel{it.second};
                    const auto& vveh{highPtPileupTrueEtMap[chid]};
                    for (int i{}; i < NBC; ++i) {
                        vvel[i].reserve(vvel[i].size() + vveh[i].size());
                        std::copy(vveh[i].begin(), vveh[i].end(), std::back_inserter(vvel[i]));
                    }
                    if (OutputBranch_PileupTrueEtSum) {
                        std::vector<double> sums;
                        sums.reserve(NBC);
                        for (const auto& it : vvel) sums.push_back(std::accumulate(it.begin(), it.end(), 0.));
                        PileupTrueEtSumMap[chid] = std::move(sums);
                    }
                    PileupTrueEtMap[chid] = std::move(vvel);
                }
            }
            if (OutputBranch_PileupTrueTau) {
                for (auto&& it : lowPtPileupTrueTauMap) {
                    const auto chid{it.first};
                    auto& vvtl{it.second};
                    const auto& vvth{highPtPileupTrueTauMap[chid]};
                    for (int i{}; i < NBC; ++i) {
                        vvtl[i].reserve(vvtl[i].size() + vvth[i].size());
                        std::copy(vvth[i].begin(), vvth[i].end(), std::back_inserter(vvtl[i]));
                    }
                    PileupTrueTauMap[chid] = std::move(vvtl);
                }
            }
        }
    }

    auto ConvADC2MeV = [](std::deque<double>& deq, Float_t lsb) {
        for (auto&& it : deq) it *= lsb;
        return deq;
    };

    const auto fg{FileGenerator(OutputFileName)};
    TFile* outputFile = new TFile(OutputFileName.c_str(), "recreate");
    TTree* outputTree = new TTree("digitSequence", "digitSequence");
    Int_t channelId;
    Float_t eta, phi;
    Int_t layer, detector;
    Float_t LSB;
    Double_t pedADC;
    Float_t noise;
    std::vector<double> SignalTrueEt;
    std::vector<double> SignalTrueTau;
    std::vector<std::vector<double>> PileupTrueEt;
    std::vector<double> PileupTrueEtSum;
    std::vector<std::vector<double>> PileupTrueTau;
    std::deque<double> AnalogEt;
    std::deque<double> SignalAnalogEt;
    std::deque<double> BgAnalogEt;
    std::deque<double> PileupAnalogEt;
    std::deque<double> ElecNoiseAnalogEt;
    std::deque<int> Digit;
    outputTree->Branch("channelId", &channelId, "channelId/I");
    outputTree->Branch("eta", &eta, "eta/F");
    outputTree->Branch("phi", &phi, "phi/F");
    outputTree->Branch("layer", &layer, "layer/I");
    outputTree->Branch("detector", &detector, "detector/I");
    outputTree->Branch("LSB", &LSB, "LSB/F");
    outputTree->Branch("pedADC", &pedADC, "pedADC/D");
    outputTree->Branch("noise", &noise, "noise/F");
    outputTree->Branch("NBC", &NBC, "NBC/I");
    outputTree->Branch("phase", &phase, "phase/F");
    outputTree->Branch("mu", &mu, "mu/D");
    outputTree->Branch("TrainPattern", &TrainPattern);
    outputTree->Branch("SignalPattern", &SignalPattern);
    outputTree->Branch("SignalTrueEt", &SignalTrueEt);
    outputTree->Branch("SignalTrueTau", &SignalTrueTau);
    outputTree->Branch("PileupTrueEt", &PileupTrueEt);
    outputTree->Branch("PileupTrueEtSum", &PileupTrueEtSum);
    outputTree->Branch("PileupTrueTau", &PileupTrueTau);
    outputTree->Branch("AnalogEt", &AnalogEt);
    outputTree->Branch("SignalAnalogEt", &SignalAnalogEt);
    outputTree->Branch("BgAnalogEt", &BgAnalogEt);
    outputTree->Branch("PileupAnalogEt", &PileupAnalogEt);
    outputTree->Branch("ElecNoiseAnalogEt", &ElecNoiseAnalogEt);
    outputTree->Branch("Digit", &Digit);

    for (auto&& i_CellConstanMap : m_CellConstantMap) {
        SampledPulse sequence{};
        SampledPulse pileupSequence{};
        channelId = i_CellConstanMap.first;
        eta       = i_CellConstanMap.second.eta;
        phi       = i_CellConstanMap.second.phi;
        layer     = i_CellConstanMap.second.layer;
        detector  = i_CellConstanMap.second.detector;
        LSB       = i_CellConstanMap.second.LSB;
        pedADC    = i_CellConstanMap.second.pedADC;
        noise     = i_CellConstanMap.second.noise;

        std::deque<double> noiseSeq;
        const double theNoise{i_CellConstanMap.second.noise};
        for (int i{}; i < NBC; ++i) noiseSeq.push_back(gRandom->Gaus(0, theNoise));
        sequence += noiseSeq;
        if (OutputBranch_ElecNoiseAnalogEt) {
            ElecNoiseAnalogEt = std::move(noiseSeq);
            ConvADC2MeV(ElecNoiseAnalogEt, LSB);
        }

        if (m_withPileup) {
            auto itLow{lowPtPileupSequenceMap.find(channelId)};
            auto itHigh{highPtPileupSequenceMap.find(channelId)};

            if (itLow == lowPtPileupSequenceMap.end() or itHigh == highPtPileupSequenceMap.end())
                Message::Fatal("There is inconsistency of channelId between GDataMap and CellConstantMap");
            sequence += (*itLow).second.rawSequence;
            sequence += (*itHigh).second.rawSequence;
            pileupSequence += (*itLow).second.rawSequence;
            pileupSequence += (*itHigh).second.rawSequence;
            if (OutputBranch_PileupAnalogEt) {
                PileupAnalogEt = pileupSequence.pulse;
                ConvADC2MeV(PileupAnalogEt, LSB);
            }

            if (OutputBranch_PileupTrueEt) PileupTrueEt = std::move(PileupTrueEtMap[channelId]);
            if (OutputBranch_PileupTrueEtSum) PileupTrueEtSum = std::move(PileupTrueEtSumMap[channelId]);
            if (OutputBranch_PileupTrueTau) PileupTrueTau = std::move(PileupTrueTauMap[channelId]);
        }

        if (OutputBranch_BgAnalogEt) {
            BgAnalogEt = sequence.pulse;
            ConvADC2MeV(BgAnalogEt, LSB);
        }

        if (m_withSignal) {
            auto itEv{signalSequenceMap.find(channelId)};
            auto& eventSequence{itEv->second};
            if (itEv not_eq signalSequenceMap.end()) {
                sequence += eventSequence.rawSequence;
                if (OutputBranch_SignalAnalogEt) {
                    SignalAnalogEt = std::move(eventSequence.rawSequence.pulse);
                    ConvADC2MeV(SignalAnalogEt, LSB);
                }
            }
            if (OutputBranch_SignalTrueEt)
                SignalTrueEt = std::move(eventSequence.SignalTrueEt);
            if (OutputBranch_SignalTrueTau)
                SignalTrueTau = std::move(eventSequence.SignalTrueTau);
        }

        if (OutputBranch_Digit) Digit = sequence.digitize();
        if (OutputBranch_AnalogEt) {
            AnalogEt = std::move(sequence.pulse);
            ConvADC2MeV(AnalogEt, LSB);
        }

        outputTree->Fill();
    }

    outputTree->Write();
    outputFile->Close();
}

void Digitizer::memberCheck()
{
    try {
        if (NBC < 0) throw "Undefined NBC";
        if (mu < 0) throw "Undefined mu";
        if (phase < 0) throw "Undefined phase";
        if (badStructure(TrainPattern)) throw "Badly defined TrainPattern";
        if (badStructure(SignalPattern)) throw "Badly defined SignalPattern";
        if (OutputFileName == "") throw "Undefined OutputFileName";
    } catch (const std::string& str) {
        Message::Fatal(str);
    }
}

std::vector<bool> Digitizer::getSignalInteractionPattern()
{
    std::vector<bool> NInteraction;
    NInteraction.reserve(NBC);
    bool bEmpty{true};
    for (int i{}; i < NBC;) {
        for (const auto& numBCinTrain : SignalPattern) {
            bEmpty = not bEmpty;
            if (bEmpty) {
                for (int j{}; j < numBCinTrain and i < NBC; ++i, ++j)
                    NInteraction.push_back(false);
            } else {
                for (int j{}; j < numBCinTrain and i < NBC; ++i, ++j)
                    NInteraction.push_back(true);
            }
            if (i >= NBC) break;
        }
    }
    if (static_cast<int>(NInteraction.size()) not_eq NBC) Message::Fatal("NInteraction.size() not_eq NBC");
    return NInteraction;
}
