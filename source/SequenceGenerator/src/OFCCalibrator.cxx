#include "OFCCalibrator.h"
#include <numeric>
#define NSAMPS 4

using namespace SequenceGenerator;

void OFCCalibrator::calibrate(bool withNoise, bool withPileup)
{
    checkSeed();
    gRandom->SetSeed(seed);
    if (not(withNoise or withPileup)) Message::Fatal("Cannot calibrate OFCs without electronic noise and pileup noise");
    memberCheck();

    setCellPropertyMaps();

    // Pileup
    HitSummaryTree lowPtPileupHSTree(LowPtPileupHitSummaryFile.c_str());
    HitSummaryTree highPtPileupHSTree(HighPtPileupHitSummaryFile.c_str());
    SequenceDataMap lowPtPileupSequenceMap{};
    SequenceDataMap highPtPileupSequenceMap{};
    if (withPileup) {
        auto pair_highlowPtNInteraction{getPileupNInteraction(PROP_HIGH)};
        const auto& highPtNInteraction = pair_highlowPtNInteraction.first;
        const auto& lowPtNInteraction  = pair_highlowPtNInteraction.second;
        lowPtPileupSequenceMap         = getSequenceMap(lowPtPileupHSTree.HitSummaryDataMapVar,
                                                        std::move(lowPtNInteraction));
        highPtPileupSequenceMap        = getSequenceMap(highPtPileupHSTree.HitSummaryDataMapVar,
                                                        std::move(highPtNInteraction));
    }

    const auto fg{FileGenerator(OutputFileName)};
    TFile* outputFile = new TFile(OutputFileName.c_str(), "recreate");
    TTree* outputTree = new TTree("OFC", "OFC");
    Int_t channelId;
    std::vector<double> a;
    std::vector<double> b;
    double pedADC;
    float LSB;
    std::vector<int> BCID;

    outputTree->Branch("channelId", &channelId, "channelId/I");
    outputTree->Branch("a", &a);
    outputTree->Branch("b", &b);
    outputTree->Branch("pedADC", &pedADC);
    outputTree->Branch("LSB", &LSB);
    outputTree->Branch("BCID", &BCID);

    for (auto&& i_CellConstanMap : m_CellConstantMap) {
        SampledPulse sequence{};
        channelId = i_CellConstanMap.first;
        pedADC    = i_CellConstanMap.second.pedADC;
        Message::Info((boost::format("Calibrating cell %1% ...") % channelId).str());
        LSB = i_CellConstanMap.second.LSB;
        {
            std::vector<int> BCIDtmp(NSAMPS, BCID0thSamp + i_CellConstanMap.second.BCshift);
            for (int i{1}; i < NSAMPS; ++i) BCIDtmp[i] += i;
            BCID = std::move(BCIDtmp);
        }

        if (withNoise) {
            std::deque<double> noise;
            const double theNoise{i_CellConstanMap.second.noise};
            for (int i{}; i < NBC; ++i)
                noise.push_back(gRandom->Gaus(0, theNoise));
            sequence += noise;
        }

        if (withPileup) {
            auto itLow{lowPtPileupSequenceMap.find(channelId)};
            auto itHigh{highPtPileupSequenceMap.find(channelId)};

            if (itLow == lowPtPileupSequenceMap.end() or itHigh == highPtPileupSequenceMap.end())
                Message::Fatal("There is inconsistency of channelId between GDataMap and CellConstantMap");
            sequence += (*itLow).second.rawSequence;
            sequence += (*itHigh).second.rawSequence;
        }

        auto itG{m_GDataMap.find(channelId)};
        if (itG == m_GDataMap.end()) Message::Fatal("There is inconsistency in m_GDataMap search");

        const std::deque<int> digits{sequence.digitize()};
        // OFCcalibration
        std::vector<double> g;
        std::vector<double> dg;
        setGdG(g, dg, itG->second.g[0], phase, itG->second.sampperbc);
        TMatrixD Rinv(NSAMPS, NSAMPS);
        a.resize(NSAMPS);
        b.resize(NSAMPS);
        if (not setRinv(Rinv, digits)) {
            for (auto&& it : a) it = 0;
            for (auto&& it : b) it = 0;
            outputTree->Fill();
            Message::Warning("Autocorrelation matrix is not invertible -> OFC vectors a and b were set as zero vectors for this cell");
            continue;
        }
        setOFC(a, b, g, dg, Rinv);
        for (auto&& it : a) { it *= LSB; }
        for (auto&& it : b) { it *= (LSB * 25.); };
        outputTree->Fill();
    }
    outputTree->Write();
    outputFile->Close();
}

void OFCCalibrator::memberCheck()
{
    try {
        if (NBC < 0) throw "Undefined NBC";
        if (mu < 0) throw "Undefined mu";
        if (phase < 0) throw "Undefined phase";
        if (badStructure(TrainPattern)) throw "Badly defined TrainPattern";
        if (OutputFileName == "") throw "Undefined OutputFileName";
        if (LowPtPileupHitSummaryFile.empty())
            throw "Undefined LowPtPileupHitSummaryFile";
        if (HighPtPileupHitSummaryFile.empty())
            throw "Undefined HighPtPileupHitSummaryFile";
    } catch (const std::string& str) {
        Message::Fatal(str);
    }
}

void OFCCalibrator::setGdG(std::vector<double>& g, std::vector<double>& dg, const std::deque<double>& pulse, const float phase, const int sampperbc)
{
    try {
        int nthSampUsed = -1;
        const double f  = sampperbc * phase;
        for (int i = 0; i < sampperbc; ++i) {
            if (f >= i * 25. and f < (i + 1) * 25.) {
                nthSampUsed = i;
                break;
            }
        }
        if (nthSampUsed < 0 or nthSampUsed >= sampperbc)
            throw "Failed to determine nthSampUsed.";

        const int nCompleteBCs = std::floor(pulse.size() / sampperbc);
        bool bJustLength       = false;
        if (NSAMPS > nCompleteBCs) {
            if (NSAMPS == nCompleteBCs + 1) {
                int size{static_cast<int>(pulse.size())};
                if (nthSampUsed < size % sampperbc)
                    bJustLength = (nthSampUsed + 1 == size % sampperbc);
                else
                    throw "The length of the original g function is too short!";
            } else
                throw "The length of the original g function is too short!";
        }

        int iSamp, i;
        for (iSamp = 0, i = nthSampUsed; iSamp < NSAMPS;
             ++iSamp, i += sampperbc) {
            g.push_back(pulse[i]);
            switch (iSamp) {
                case 0:
                    if (not nthSampUsed) {
                        dg.push_back(
                          pulse[i + 1] * 10.); // scaling is arbitrary (temporary: 10.)
                        continue;
                    } else
                        break;
                case NSAMPS - 1:
                    if (bJustLength) {
                        dg.push_back(
                          -pulse[i - 1] * 10.); // scaling is arbitrary (temporary: 10.)
                        continue;
                    }
            }
            dg.push_back((pulse[i + 1] - pulse[i - 1]) * 10.); // scaling is arbitrary (temporary: 10.)
        }

        if (g.size() not_eq NSAMPS or dg.size() not_eq NSAMPS)
            throw "g.size() not_eq NSAMPS or dg.size() not_eq NSAMPS";
    } catch (const std::string& str) {
        Message::Fatal(str);
    }
}

bool OFCCalibrator::setRinv(TMatrixD& Rinv, const std::deque<int>& digits)
{
    const double size = static_cast<double>(digits.size());
    const auto ave    = std::accumulate(std::begin(digits), std::end(digits), 0.) / size;
    // const auto var = std::inner_product(std::begin(digits), std::end(digits),
    // std::begin(digits), 0.0) / size - ave * ave;
    int nIterate = size - NSAMPS + 1;
    std::vector<double> R(NSAMPS);
    for (int i = 0; i < nIterate; ++i) {
        for (int j = 0; j < NSAMPS; ++j)
            R[j] += (digits[i] - ave) * (digits[i + j] - ave);
    }
    for (auto&& it : R) it *= .00001; // scaling is arbitrary
    for (int i = 0; i < NSAMPS; ++i) {
        for (int j = 0; j < NSAMPS - i; ++j)
            Rinv(j, j + i) = Rinv(j + i, j) = R[i];
    }
    // Rinv.Print();
    Double_t det{};
    Rinv.Invert(&det);
    // Rinv.Print();
    if (det)
        return true;
    else
        return false;
}

bool OFCCalibrator::setRinv(TMatrixD& Rinv, const std::deque<double>& digits)
{
    const double size = static_cast<double>(digits.size());
    const auto ave    = std::accumulate(std::begin(digits), std::end(digits), 0.) / size;
    // const auto var = std::inner_product(std::begin(digits), std::end(digits),
    // std::begin(digits), 0.0) / size - ave * ave;
    int nIterate = size - NSAMPS + 1;
    std::vector<double> R(NSAMPS);
    for (int i = 0; i < nIterate; ++i) {
        for (int j = 0; j < NSAMPS; ++j)
            R[j] += (digits[i] - ave) * (digits[i + j] - ave);
    }
    for (auto&& it : R) it *= .00001; // scaling is arbitrary
    for (int i = 0; i < NSAMPS; ++i) {
        for (int j = 0; j < NSAMPS - i; ++j)
            Rinv(j, j + i) = Rinv(j + i, j) = R[i];
    }
    // Rinv.Print();
    Double_t det{};
    Rinv.Invert(&det);
    // Rinv.Print();
    if (det)
        return true;
    else
        return false;
}

void OFCCalibrator::setOFC(std::vector<double>& a, std::vector<double>& b, const std::vector<double>& g, const std::vector<double>& dg, const TMatrixD& Rinv)
{
    // for (auto&& it : g) std::cout << it << "\t"; std::cout << "\n";
    // for (auto&& it : dg) std::cout << it << "\t"; std::cout << "\n";
    // Rinv.Print();
    std::vector<double> Rinv_g;
    std::vector<double> Rinv_dg;
    for (int i = 0; i < NSAMPS; ++i) {
        Rinv_g.push_back(
          std::inner_product(g.begin(), g.end(), Rinv[i].GetPtr(), 0.));
        Rinv_dg.push_back(
          std::inner_product(dg.begin(), dg.end(), Rinv[i].GetPtr(), 0.));
    }
    const double Q00 = std::inner_product(g.begin(), g.end(), Rinv_g.begin(), 0.);
    const double Q01 = std::inner_product(g.begin(), g.end(), Rinv_dg.begin(), 0.);
    // const double Q01 = std::inner_product(dg.begin(), dg.end(),
    // Rinv_g.begin(), 0.);
    const double Q11    = std::inner_product(dg.begin(), dg.end(), Rinv_dg.begin(), 0.);
    const double Del    = Q00 * Q11 - Q01 * Q01;
    const double lambda = Q11 / Del;
    // const double kappa = -Q01 / Del;
    const double mu  = Q01 / Del;
    const double rho = -Q00 / Del;
    for (int i = 0; i < NSAMPS; ++i) {
        a[i] = lambda * Rinv_g[i] - mu * Rinv_dg[i];
        b[i] = mu * Rinv_g[i] + rho * Rinv_dg[i];
    }
}
