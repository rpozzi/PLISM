#include "OFCPhysTree.h"
#include <TInterpreter.h>

using namespace SequenceGenerator;

void OFCPhysTree::CheckMembers()
{
    if (NthPhase < 0) Message::Fatal("NthPhase is not set");
    if (OFBCshift < 0) { // temporary (will be read from the tree in the future, hopefully)
        Message::Fatal("OFBCshift is not set");
    }
    if (OutputFileName.empty()) Message::Fatal("OutputFileName is not set");
    if (CellPropertyFileName.empty()) Message::Fatal("CellPropertyFileName is not set");
}

void OFCPhysTree::MakeTree()
{
    CheckMembers();

    CellPropertyTree CellProperty(CellPropertyFileName.c_str());
    CellProperty.setMaps();

    const auto fg{FileGenerator(OutputFileName)};
    TFile* outputFile = new TFile(OutputFileName.c_str(), "recreate");
    TTree* outputTree = new TTree("OFC", "OFC");
    Int_t channelId_out;
    std::vector<double> a;
    std::vector<double> b;
    double pedADC;
    float LSB;
    std::vector<int> BCID;
    int phase;
    bool isCali {false};

    outputTree->Branch("channelId", &channelId_out, "channelId/I");
    outputTree->Branch("a", &a);
    outputTree->Branch("b", &b);
    outputTree->Branch("pedADC", &pedADC);
    outputTree->Branch("LSB", &LSB);
    outputTree->Branch("BCID", &BCID);
    outputTree->Branch("OFBCshift", &OFBCshift);
    outputTree->Branch("phase", &phase);
    outputTree->Branch("isCali", &isCali);
    Restart();
    while (Next()) {
        if (*Phase not_eq NthPhase) continue;

        a.clear();
        a.shrink_to_fit();
        b.clear();
        b.shrink_to_fit();
        channelId_out = *channelId;
        auto itr{CellProperty.CellConstantMapVar.find(*channelId)};
        if (itr == CellProperty.CellConstantMapVar.end()) continue;
        pedADC = itr->second.pedADC;
        LSB    = itr->second.LSB;
        for (int i{}; i < *nSamples; ++i) {
            a.push_back(OFCa[i] * LSB);
            b.push_back(OFCb[i] * LSB);
        }
        phase = NthPhase;

        outputTree->Fill();
    }

    outputTree->Write();
    outputFile->Close();
}
/*
void OFCPhysTree::MakegFuncOFCTree()
{
    CheckMembers();

    gInterpreter->GenerateDictionary("std::vector<std::deque<double> >",
                                     "vector;deque");
    CellPropertyTree CellProperty(CellPropertyFileName.c_str());
    CellProperty.setMaps();
    const auto fg{FileGenerator(OutputFileName)};
    TFile* outputFile = new TFile(OutputFileName.c_str(), "recreate");
    TTree* outputTree = new TTree("OFC", "OFC");

    Int_t channelId_out;
    Float_t eta_out;
    Float_t phi_out;
    Int_t layer_out;
    std::vector<double> a;
    std::vector<double> b;
    Float_t LSB;
    Int_t sampperbc;
    Int_t Phase_out;
    Int_t nSamples_out;
    std::vector<float> inputET;
    std::vector<std::deque<double>> gFunctions;
    std::vector<std::deque<double>> NgFunctions;
    std::vector<std::deque<double>> NgFunction_OFCa;
    std::vector<std::deque<double>> NgFunction_OFCb;
    std::vector<std::deque<double>> tau;
    std::vector<double> peakEt;
    std::vector<double> peakTau;
    bool isCali {false};

    outputTree->Branch("channelId", &channelId_out, "channelId/I");
    outputTree->Branch("eta", &eta_out);
    outputTree->Branch("phi", &phi_out);
    outputTree->Branch("layer", &layer_out);
    outputTree->Branch("OFCa", &a);
    outputTree->Branch("OFCb", &b);
    outputTree->Branch("LSB", &LSB);
    outputTree->Branch("sampperbc", &sampperbc);
    outputTree->Branch("Phase", &Phase_out);
    outputTree->Branch("nSamples", &nSamples_out);
    outputTree->Branch("OFBCshift", &OFBCshift);
    outputTree->Branch("inputET", &inputET);
    outputTree->Branch("gFunction", &gFunctions);
    outputTree->Branch("NgFunction", &NgFunctions);
    outputTree->Branch("NgFunctionOFCa", &NgFunction_OFCa);
    outputTree->Branch("NgFunctionOFCb", &NgFunction_OFCb);
    outputTree->Branch("tau", &tau);         // recoEtTau/Et
    outputTree->Branch("peakEt", &peakEt);   // peak amplitude of NgFunction*OFCa
    outputTree->Branch("peakTau", &peakTau); // Tau of peak point
    outputTree->Branch("isCali", &isCali);

    Restart();
    while (Next()) {
        if (*Phase not_eq NthPhase) continue;
        a.clear();
        a.shrink_to_fit();
        b.clear();
        b.shrink_to_fit();
        gFunctions.clear();
        gFunctions.shrink_to_fit();
        NgFunctions.clear();
        NgFunctions.shrink_to_fit();
        NgFunction_OFCa.clear();
        NgFunction_OFCa.shrink_to_fit();
        NgFunction_OFCb.clear();
        NgFunction_OFCb.shrink_to_fit();
        tau.clear();
        tau.shrink_to_fit();

        channelId_out = *channelId;
        auto itr{CellProperty.CellConstantMapVar.find(*channelId)};
        if (itr == CellProperty.CellConstantMapVar.end()) continue;
        eta_out = itr->second.eta;
        phi_out = itr->second.phi;
        layer_out = itr->second.layer;
        LSB     = itr->second.LSB;
        for (int i{}; i < *nSamples; ++i) {
            a.push_back(OFCa[i]);
            b.push_back(OFCb[i]);
        }
        auto itr1{CellProperty.GDataMapVar.find(*channelId)};
        if (itr1 == CellProperty.GDataMapVar.end()) continue;
        sampperbc    = itr1->second.sampperbc;
        Phase_out    = NthPhase;
        nSamples_out = *nSamples;
        inputET      = itr1->second.ET;
        gFunctions   = itr1->second.g;

        NgFunctions.resize(gFunctions.size());
        auto itr2{NgFunctions.begin()};
        for (const auto& gFunc : gFunctions) {
            double max = *std::max_element(gFunc.begin(), gFunc.end());
            for (const auto& g : gFunc) itr2->push_back(g / max);
            ++itr2;
        }

        NgFunction_OFCa.resize(NgFunctions.size());
        NgFunction_OFCb.resize(NgFunctions.size());
        tau.resize(NgFunctions.size());

        auto itrA{NgFunction_OFCa.begin()};
        auto itrB{NgFunction_OFCb.begin()};
        auto itr3{tau.begin()};

        const auto loop = NgFunctions[0].size() - sampperbc * (*nSamples - 1);
        for (const auto& NgFunc : NgFunctions) {
            for (std::size_t n{}; n <= loop; n += sampperbc) {
                auto Et    = 0.;
                auto EtTau = 0.;
                for (int i{}; i < *nSamples; i++) {
                    Et += NgFunc[n + i * sampperbc] * OFCa[i];
                    EtTau += NgFunc[n + i * sampperbc] * OFCb[i];
                }
                itrA->push_back(Et);
                itrB->push_back(EtTau);
                itr3->push_back(EtTau / Et);
            }
            ++itrA;
            ++itrB;
            ++itr3;
        }

        itr3 = tau.begin();

        for (const auto& NgFuncA : NgFunction_OFCa) {
            auto peakET = *std::max_element(NgFuncA.begin(), NgFuncA.end());
            peakEt.push_back(peakET);
            auto itr_peak = std::find(NgFuncA.begin(), NgFuncA.end(), peakET);
            if (itr_peak == NgFuncA.end())
                continue;
            else {
                const auto index = std::distance(NgFuncA.begin(), itr_peak);
                peakTau.push_back(itr3->at(index));
            }
            ++itr3;
        }

        outputTree->Fill();
    }
    outputTree->Write();
    outputFile->Close();
}
*/
